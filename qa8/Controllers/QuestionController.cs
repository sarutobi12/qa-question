﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using qa8.Data;
using qa8.Models;
using qa8.Repository;

namespace qa8.Controllers
{
    [Authorize]
    public class QuestionController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        public QuestionController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: Question
        [Authorize]
        public async Task<IActionResult> Index()
        {
            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                var quizContext = _context.Questions.Include(q => q.quiz);
                return View(await quizContext.ToListAsync());
            }
            return View("~/Views/Account/AccessDenied.cshtml");
            
        }

        // GET: Question/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var question = await _context.Questions
                .Include(q => q.quiz)
                .SingleOrDefaultAsync(m => m.id == id);
            if (question == null)
            {
                return NotFound();
            }
            ViewData["quizId"] = question.quizId;
            return View(question);
        }

        // GET: Question/Create
        // GET: Question/Create
        [Authorize]
        public async Task<IActionResult> Create(int quizId)
        {
            if (quizId != 0)
            {
                ViewData["quizId"] = quizId;
            }
            else
            {
                int id = Convert.ToInt32(TempData["quizId"]);
                ViewData["quizId"] = id;
            }

            Quiz quiz = await _context.Quizzes.Where(p => p.id == quizId).AsNoTracking().SingleAsync();
            //ViewData["text"] = new SelectList(_context.Quizzes, "text", "text");
            ViewData["text"] = quiz.text;

            return View();
        }

        // POST: Question/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("id,quizId,text,A,B,C,D,E,correct,multipleChoice,imageSource,imageSourceA,imageSourceB,imageSourceC,imageSourceD,imageSourceE")] Question question)
        {

            if (ModelState.IsValid)
            {
                _context.Add(question);
                await _context.SaveChangesAsync();
                //TempData["quizId"] = question.quizId;
                return RedirectToAction("Details","Quiz", new { id = question.quizId });
            }
            ViewData["quizId"] = new SelectList(_context.Quizzes, "id", "id", question.quizId);
            //TempData["quizId"] = question.quizId;
            //return RedirectToAction(nameof(Create));
            return View(question);
        }

        // GET: Question/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var question = await _context.Questions.SingleOrDefaultAsync(m => m.id == id);
            if (question == null)
            {
                return NotFound();
            }
            ViewData["quizId"] = question.quizId;
            ViewData["quizText"] = question.text;
            return View(question);
        }

        // POST: Question/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("id,quizId,text,A,B,C,D,E,correct,multipleChoice,imageSource,imageSourceA,imageSourceB,imageSourceC,imageSourceD,imageSourceE")] Question question)
        {
            if (id != question.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(question);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuestionExists(question.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                
            }
            //ViewData["quizId"] = new SelectList(_context.Quizzes, "quizId", "quizName", question.quizId);
            
            return RedirectToAction("Details", "Quiz", new { id = question.quizId });
        }

        // GET: Question/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var question = await _context.Questions
                .Include(q => q.quiz)
                .SingleOrDefaultAsync(m => m.id == id);
            if (question == null)
            {
                return NotFound();
            }
            ViewData["quizId"] = question.quizId;
            return View(question);
        }

        // POST: Question/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var question = await _context.Questions.SingleOrDefaultAsync(m => m.id == id);
            _context.Questions.Remove(question);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Quiz", new { id = question.quizId });
        }

        private bool QuestionExists(int id)
        {
            return _context.Questions.Any(e => e.id == id);
        }


        public async Task<ActionResult> Upload(IFormFile file)
        {

            //IFormFile file = Request.Form.Files[0];
            string folderName = "images";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            

            // string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = file.FileName;
            //FileInfo fileInfo = new FileInfo(Path.Combine(newPath, fileName));
            string result = string.Concat(fileName.Where(c => !char.IsWhiteSpace(c)));

            ////< init >

            //long uploaded_size = 0;
            //string path_for_Uploaded_Files = _hostingEnvironment.WebRootPath + "\\images";
            ////</ init >
            ////< get form_files >
            ////IFormFile uploaded_File
            //var uploaded_file = file;

            ////</ get form_files >
            ////------< @Loop: Uploaded Files >------
            //int iCounter = 0;
            //string sFiles_uploaded = "";
            ////foreach (var uploaded_file in uploaded_files)
            ////{
            ////    //----< Uploaded File >----

            ////    iCounter++;
            ////    uploaded_size += uploaded_file.Length;
            ////    sFiles_uploaded += "\n" + uploaded_file.FileName;
            ////    //< Filename >
            ////    string uploaded_Filename = uploaded_file.FileName;
            ////    string new_Filename_on_Server = path_for_Uploaded_Files + "\\" + uploaded_Filename;
            ////    //</ Filename >
            ////    //< Copy File to Target >
            ////    using (FileStream stream = new FileStream(new_Filename_on_Server, FileMode.Create))

            ////    {

            ////        await uploaded_file.CopyToAsync(stream);

            ////    }
            ////    //< Copy File to Target >
            ////    //----</ Uploaded File >----
            ////}
            ////------</ @Loop: Uploaded Files >------
            //uploaded_size += uploaded_file.Length;
            //sFiles_uploaded += "\n" + uploaded_file.FileName;
            ////< Filename >
            Guid id = Guid.NewGuid();
            string new_Filename = id.ToString("N")+ fileName;
            string new_Filename_on_Server = newPath + "\\" + new_Filename;
            //</ Filename >
            //< Copy File to Target >
            FileStream stream = new FileStream(new_Filename_on_Server, FileMode.Create);
            await file.CopyToAsync(stream);
            var message = "/images/" + id.ToString("N") + fileName;
            
            return Json(message);
        }
    }
}
