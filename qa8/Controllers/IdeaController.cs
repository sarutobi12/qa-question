﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using qa8.Data;
using qa8.Models;
using qa8.Repository;

namespace qa8.Controllers
{
    [Authorize]
    public class IdeaController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        //----------------------------------------------------------------------

        public IdeaController(
            ApplicationDbContext context, 
            IHostingEnvironment hostingEnvironment,
            UserManager<ApplicationUser> userManager
            )
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
        }

        //--------------------
        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }
        // GET: Idea
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            //// khu vuc danh cho cap 4
            var controllerPolicy = 4;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["DateSortParm"] = String.IsNullOrEmpty(sortOrder) ? "date_increase" : "";
                ViewData["NameSortParm"] = sortOrder == "Name" ? "name_desc" : "Name";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewData["CurrentFilter"] = searchString;

                var ideas = from s in _context.Idea
                                  select s;
                if (!String.IsNullOrEmpty(searchString))
                {
                    ideas = ideas.Where(s => s.idea_Title.Contains(searchString)
                                           || Convert.ToDateTime(s.date_Created) >= Convert.ToDateTime(searchString));
                }

                switch (sortOrder)
                {
                    case "date_increase":
                        ideas = ideas.OrderBy(s => s.date_Created);
                        break;
                    case "Name":
                        ideas = ideas.OrderBy(s => s.idea_Title);
                        break;
                    case "name_desc":
                        ideas = ideas.OrderByDescending(s => s.idea_Title);
                        break;
                    default:
                        ideas = ideas.OrderByDescending(s => s.date_Created);
                        break;
                }
                int pageSize = 10;
                return View(await PaginatedList<Idea>.CreateAsync(ideas.AsNoTracking(), page ?? 1, pageSize));

                //return View(await _context.Idea.ToListAsync());
            }
            return View("~/Views/Account/AccessDenied.cshtml");
            
        }

        // GET: Idea/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //// khu vuc danh cho cap 4
            var controllerPolicy = 4;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                var idea = await _context.Idea
                .SingleOrDefaultAsync(m => m.id == id);
                if (idea == null)
                {
                    return NotFound();
                }

                return View(idea);
            }
            return View("~/Views/Account/AccessDenied.cshtml");

            
        }
        [AllowAnonymous]
        // GET: Idea/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Idea/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,idea_Title,idea_Content,date_Created,ip_Created")] Idea idea)
        {

            idea.date_Created = DateTime.Now.ToString();
            

            if (ModelState.IsValid)
            {
                _context.Add(idea);
                await _context.SaveChangesAsync();
                return View("~/Views/Idea/Thankyou.cshtml");
                //return RedirectToAction(nameof(Index));
            }
            return View(idea);
        }

        // GET: Idea/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var idea = await _context.Idea.SingleOrDefaultAsync(m => m.id == id);
        //    if (idea == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(idea);
        //}

        // POST: Idea/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("id,idea_Title,idea_Content,date_Created,ip_Created")] Idea idea)
        //{
        //    if (id != idea.id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(idea);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!IdeaExists(idea.id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(idea);
        //}

        // GET: Idea/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //// khu vuc danh cho cap 5
            var controllerPolicy = 5;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                var idea = await _context.Idea
                .SingleOrDefaultAsync(m => m.id == id);
                if (idea == null)
                {
                    return NotFound();
                }

                return View(idea);
            }
            return View("~/Views/Account/AccessDenied.cshtml");

            
        }

        // POST: Idea/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var idea = await _context.Idea.SingleOrDefaultAsync(m => m.id == id);
            _context.Idea.Remove(idea);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool IdeaExists(int id)
        {
            return _context.Idea.Any(e => e.id == id);
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

       
    }
}
