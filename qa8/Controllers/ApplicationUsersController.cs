﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using qa8.Data;
using qa8.Models;
using qa8.Models.AccountViewModels;
using qa8.Services;
using qa8.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.IO;
using OfficeOpenXml;
using System.Text.RegularExpressions;
using ExcelDataReader;
using NPOI;
using qa8.Models.ViewModel;
using qa8.Datalayer;

namespace qa8.Controllers
{
    [Authorize]
    public class ApplicationUsersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ApplicationUsersController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            ApplicationDbContext context,
            IHostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }


        //GET : 
        public async Task<IActionResult> UploadEmployeeList()
        {
            //// khu vuc danh cho cap 4
            var controllerPolicy = 4;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                return View();
            }
            return View("~/Views/Account/AccessDenied.cshtml");

        }


        //Post :
        [HttpPost]
        public async Task<IActionResult> UploadEmployeeListUpload()
        {
            //// khu vuc danh cho cap 4
            var controllerPolicy = 4;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                var users = await _context.ApplicationUser.ToListAsync();
                List<string> existUserNameList = new List<string>();
                foreach (var u in users)
                {
                    var checkAccountExist = existUserNameList.Contains(u.UserName);
                    if (checkAccountExist == false)
                    {
                        existUserNameList.Add(u.UserName);
                    }
                }
                var x = existUserNameList;

                IFormFile file = Request.Form.Files[0];
                string folderName = "accountExcel";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                StringBuilder sb = new StringBuilder();
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                }
                
                // string rootFolder = _hostingEnvironment.WebRootPath;
                string fileName = file.FileName;
                //var fileNameNormalize = Regex.Replace(fileName, "[^0-9a-zA-Z]+", "");
                //FileInfo fileInfo = new FileInfo(Path.Combine(newPath, fileName));
                string newnewPath = Path.Combine(newPath, fileName);


                using (var stream = new FileStream(newnewPath, FileMode.Open))
                {
                    //var memoryStream = new MemoryStream();
                    //await file.CopyToAsync(stream).ConfigureAwait(true);


                    file.CopyTo(stream);
                    stream.Position = 0;
                    using (ExcelPackage package = new ExcelPackage(stream))
                    {
                        //int totalWorksheet = package.Workbook.Worksheets.Count;
                        //ExcelWorksheet workSheet = package.Workbook.Worksheets["Sheet1"];

                        //int totalColums = workSheet.Dimension.End.Column;
                        //int totalRows = workSheet.Dimension.End.Row;

                        List<ApplicationUser> employeeList = new List<ApplicationUser>();

                        //int j = 1;
                        //for (int i = 1; i <= totalRows; i++)
                        //{
                        //    var newSOP = new ApplicationUser();

                        //    var userName = (workSheet.Cells[i, 1].Value ?? "").ToString();

                        //    var fullName = (workSheet.Cells[i, 2].Value ?? "").ToString();
                        //    var engName = (workSheet.Cells[i, 3].Value ?? "").ToString();

                        //    newSOP.UserName = userName;
                        //    newSOP.fullName = fullName;
                        //    newSOP.nickName = engName;


                        //    employeeList.Add(newSOP);

                        //}


                        //new code here
                        var d = new DataLayer();
                        var ds = d.ReadSach();
                        for (int i = 0; i < ds.Count; i++)
                        {
                            var newSOP = new ApplicationUser();

                            //var userName = (workSheet.Cells[i, 1].Value ?? "").ToString();

                            //var fullName = (workSheet.Cells[i, 2].Value ?? "").ToString();
                            //var engName = (workSheet.Cells[i, 3].Value ?? "").ToString();

                            var userName = ds[i].cardId;

                            var fullName = ds[i].fullName;
                            //var engName = ds[i].EnglishName;

                            newSOP.UserName = userName;
                            newSOP.fullName = fullName;
                            //newSOP.nickName = engName;


                            employeeList.Add(newSOP);

                        }
                        //end

                        int count = employeeList.Count();
                        int count1 = existUserNameList.Count();
                        for (var i = 0; i < count; i++)
                        {

                            var checkAccountExistBeforeSave = existUserNameList.Contains(employeeList[i].UserName);
                            if (checkAccountExistBeforeSave == false)
                            {

                                var Password = "shc1234";
                                var result = await _userManager.CreateAsync(employeeList[i], Password);
                            }

                        }

                        sb.Append("<t class='text-info'>Upload succesfully </h3>");
                    }
                }

                //


                //--------------------------------------------------

                //FileStream stream = File.Open(fileInfo, FileMode.Open, FileAccess.Read);
                #region abc
                //using (var memoryStream = new MemoryStream())
                //{
                //    await file.CopyToAsync(memoryStream).ConfigureAwait(true);

                //    //using (var package = new ExcelPackage(memoryStream))
                //    //{
                //    //    var worksheet = package.Workbook.Worksheets[1]; // Tip: To access the first worksheet, try index 1, not 0
                //    //    return Content(readExcelPackageToString(package, worksheet));
                //    //}
                //    using (ExcelPackage package = new ExcelPackage(memoryStream))
                //    {
                //        int totalWorksheet = package.Workbook.Worksheets.Count;
                //        ExcelWorksheet workSheet = package.Workbook.Worksheets["NS0922"];

                //        int totalColums = workSheet.Dimension.End.Column;
                //        int totalRows = workSheet.Dimension.End.Row;

                //        List<ApplicationUser> employeeList = new List<ApplicationUser>();

                //        //int j = 1;
                //        for (int i = 1; i <= totalRows; i++)
                //        {
                //            var newSOP = new ApplicationUser();

                //            var userName = (workSheet.Cells[i, 1].Value ?? "").ToString();

                //            var fullName = (workSheet.Cells[i, 4].Value ?? "").ToString();


                //            newSOP.UserName = userName;
                //            newSOP.fullName = fullName;


                //            employeeList.Add(newSOP);

                //        }

                //        int count = employeeList.Count();
                //        int count1 = existUserNameList.Count();
                //        for (var i = 0; i < count; i++)
                //        {

                //            var checkAccountExistBeforeSave = existUserNameList.Contains(employeeList[i].UserName);
                //            if (checkAccountExistBeforeSave == false)
                //            {

                //                var Password = "shc1234";
                //                var result = await _userManager.CreateAsync(employeeList[i], Password);
                //            }

                //        }

                //        sb.Append("<t class='text-info'>Upload succesfully </h3>");
                //    }
                //}
                #endregion abc


                //----------------------------------------

                return this.Content(sb.ToString());
            }
            return View("~/Views/Account/AccessDenied.cshtml");

        }

        // GET: ApplicationUsers
        public async Task<IActionResult> Index()
        {
            //// khu vuc danh cho cap 4
            var controllerPolicy = 4;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                return View(await _context.ApplicationUser.ToListAsync());
            }
            return View("~/Views/Account/AccessDenied.cshtml");

        }

        // GET: ApplicationUsers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //// khu vuc danh cho cap 5
            var controllerPolicy = 5;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                var applicationUser = await _context.ApplicationUser
                .SingleOrDefaultAsync(m => m.Id == id);
                if (applicationUser == null)
                {
                    return NotFound();
                }

                return View(applicationUser);
            }

            return View("~/Views/Account/AccessDenied.cshtml");


        }

        // GET: ApplicationUsers/Create
        public async Task<IActionResult> Create()
        {
            //// khu vuc danh cho cap 4
            var controllerPolicy = 4;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                return View();
            }

            return View("~/Views/Account/AccessDenied.cshtml");

        }

        // POST: ApplicationUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddUserViewModel model)
        {
            var user = new ApplicationUser { UserName = model.UserName, fullName = model.fullName, accountLevel = "1" };

            try
            {
                var Password = "shc1234";
                var result = await _userManager.CreateAsync(user, Password);
                return RedirectToAction(nameof(Index));
            }
            catch
            {

            }
            return View(user);
        }

        // GET: ApplicationUsers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            //// khu vuc danh cho cap 5
            var controllerPolicy = 5;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                var applicationUser = await _context.ApplicationUser.SingleOrDefaultAsync(m => m.Id == id);
                if (applicationUser == null)
                {
                    return NotFound();
                }
                return View(applicationUser);
            }

            return View("~/Views/Account/AccessDenied.cshtml");
        }

        // POST: ApplicationUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("departmentCode,cardNumber,fullName,startDate,nickName,accountLevel,Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount")] ApplicationUser applicationUser)
        {
            if (id != applicationUser.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(applicationUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationUserExists(applicationUser.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(applicationUser);
        }

        // GET: ApplicationUsers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //// khu vuc danh cho cap 5
            var controllerPolicy = 5;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                var applicationUser = await _context.ApplicationUser
                .SingleOrDefaultAsync(m => m.Id == id);
                if (applicationUser == null)
                {
                    return NotFound();
                }

                return View(applicationUser);
            }

            return View("~/Views/Account/AccessDenied.cshtml");


        }

        // POST: ApplicationUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var applicationUser = await _context.ApplicationUser.SingleOrDefaultAsync(m => m.Id == id);
            _context.ApplicationUser.Remove(applicationUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApplicationUserExists(int id)
        {
            return _context.ApplicationUser.Any(e => e.Id == id);
        }
        
    }
}
