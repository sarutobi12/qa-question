﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using qa8.Data;
using qa8.Models;
using qa8.Models.ViewModel;

namespace qa8.Controllers
{
    [Produces("application/json")]
    [Route("api/Quizs")]
    public class QuizsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public QuizsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: api/Quizs
        [HttpGet]
        public IEnumerable<Quiz> GetQuizzes()
        {
            return _context.Quizzes;
        }

        //[HttpGet]
        //public IEnumerable<QuizDetailViewModel> QuizzesResult()
        //{
        //    int id = 1;
           
        //    var quiz =  _context.Quizzes.Where(a => a.id == id).AsNoTracking().SingleOrDefault();

        //    //retreive all answersheet having same quizID
        //    var ListOfAnswerSheet =  _context.AnswerSheets.Where(a => a.quizId == id).AsNoTracking().ToList();
        //    //
        //    List<int> listOfUserId = new List<int>();
        //    //for (var i = 0; i < answerSheet.Count; i++)
        //    //{
        //    //    if (listOfUserId.Contains(answerSheet[i].userId) == false)
        //    //    {   
        //    //        listOfUserId.Add(answerSheet[i].userId);
        //    //    }
        //    //}
        //    //for (var i = 0; i < listOfUserId.Count; i++)
        //    //{
        //    //    var NumberOfmulipleChoiceQuestion = 0;
        //    //    var correctAnswerOfMulipleChoiceQuestion = 0;
        //    //    for (var j = 0; j< answerSheet.Count; j++)
        //    //    {
        //    //        if(answerSheet[j].userId == listOfUserId[i])
        //    //    }
        //    //}



        //    //list of quizdetailviewmodel contain result
        //    List<QuizDetailViewModel> listOfQuizDetailViewModel = new List<QuizDetailViewModel>();
        //    //list of answer sheet
        //    //List<AnswerSheet> listOfAnswerSheet = new List<AnswerSheet>();
        //    ////retreive all answersheet having same quizId
        //    //listOfAnswerSheet = await _context.AnswerSheets
        //    //    .Where(a => a.quizId == id).ToListAsync();
        //    foreach (AnswerSheet answerSheet in ListOfAnswerSheet)
        //    {
        //        //using findingresult method to make a quiz detail view model
        //        QuizDetailViewModel quizDetailViewModel =  FindingResultByUserId(answerSheet);
        //        int targetScore = Int32.Parse(quiz.targetScore);
        //        if (quizDetailViewModel.score > targetScore)
        //        {
        //            quizDetailViewModel.result = "Passed";
        //        }
        //        else
        //        {
        //            quizDetailViewModel.result = "Fail";
        //        }
        //        var userInfo =  _context.Users.Where(u => u.Id == quizDetailViewModel.userId).AsNoTracking().SingleOrDefault();
        //        quizDetailViewModel.userName = userInfo.UserName;
        //        quizDetailViewModel.fullName = userInfo.fullName;

        //        //check if  quiz detail view model is exist
        //        bool check = CheckQuizDetailViewModelExist(listOfQuizDetailViewModel, quizDetailViewModel);
        //        // if list is empty or quiz not exist in list , then add quiz detail to list
        //        if (check == false)
        //        {
        //            listOfQuizDetailViewModel.Add(quizDetailViewModel);
        //        }
        //    }
        //    //Result result = new Result();
        //    //if (listOfQuizDetailViewModel.Count > 0)
        //    //{
        //    //    result = ResultStatistic(listOfQuizDetailViewModel);
        //    //}
        //    //List<QuizDetailViewModel> listlistOfQuizDetailViewModelSorted = new List<QuizDetailViewModel>();
        //    //listlistOfQuizDetailViewModelSorted = MergeSort(listOfQuizDetailViewModel);

        //    //ViewBag.result = result;
        //    return listOfQuizDetailViewModel;
        //}

        // recieve a answersheet  , then count all answersheet having same quizid and userid
        // then provide the result as QuizDetailViewModel
        //public  QuizDetailViewModel FindingResultByUserId(AnswerSheet answerSheet)
        //{
        //    int sum = 0;
        //    int count = 0;
        //    var quizDetailViewModel = new QuizDetailViewModel();
        //    var DateSubmitted = answerSheet.dateSubmitted;

        //    //retreive all answersheet having same quizId and userId
        //    var answerSheetContext =  _context.AnswerSheets
        //        .Where(a => a.quizId.Equals(answerSheet.quizId) && a.userId.Equals(answerSheet.userId))
        //        .AsNoTracking().ToList();

        //    foreach (AnswerSheet answerSheet1 in answerSheetContext)
        //    {

        //        //bool type = await FindingQuestionType(answerSheet1.questionId);
        //        if (answerSheet1.questionType == true)
        //        {
        //            sum += 1;
        //        }

        //        if (answerSheet1.isCorrect == true && answerSheet1.questionType == true)
        //        {
        //            count += 1;
        //        }
        //        if (DateSubmitted.ToString < answerSheet1.dateSubmitted)
        //        {
        //            DateSubmitted = answerSheet1.dateSubmitted;
        //        }
        //    }
        //    quizDetailViewModel.userId = answerSheet.userId;
        //    quizDetailViewModel.submittionDateTime = DateSubmitted;
        //    quizDetailViewModel.score = count * 100 / sum;
        //    return quizDetailViewModel;
        //}

        public bool CheckQuizDetailViewModelExist(List<QuizDetailViewModel> listOfQuizDetailVM, QuizDetailViewModel quizDetailViewModel)
        {
            bool check = false;
            for (int i = 0; i < listOfQuizDetailVM.Count; i++)
            {
                if (quizDetailViewModel.userId == listOfQuizDetailVM[i].userId)
                {
                    check = true;
                }
            }
            return check;
        }

        // GET: api/Quizs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetQuiz([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var quiz = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);

            if (quiz == null)
            {
                return NotFound();
            }

            return Ok(quiz);
        }

        // PUT: api/Quizs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuiz([FromRoute] int id, [FromBody] Quiz quiz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != quiz.id)
            {
                return BadRequest();
            }

            _context.Entry(quiz).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuizExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Quizs
        [HttpPost]
        public async Task<IActionResult> PostQuiz([FromBody] Quiz quiz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Quizzes.Add(quiz);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetQuiz", new { id = quiz.id }, quiz);
        }

        // DELETE: api/Quizs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteQuiz([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var quiz = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);
            if (quiz == null)
            {
                return NotFound();
            }

            _context.Quizzes.Remove(quiz);
            await _context.SaveChangesAsync();

            return Ok(quiz);
        }

        private bool QuizExists(int id)
        {
            return _context.Quizzes.Any(e => e.id == id);
        }
    }
}