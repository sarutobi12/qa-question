﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Npoi.Core.SS.UserModel;
using Npoi.Core.XSSF.UserModel;
using qa8.Data;
using qa8.Extensions;
using qa8.Models;
using qa8.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using qa8.Repository;
using System.Diagnostics;

namespace qa8.Controllers
{
    public class QuizController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        //----------------------------------------------------------------------

        public QuizController(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager, 
            IHostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        //----------------------------------------------------------------------

        const string UserConnected = "UserConnected";
        const string SelectedQuiz = "SelectedQuiz";
        const string quizId = "quizId";
        

        //---------upload an image----------------------------------------------------
        public string UploadImage(string fullName, IFormFile image)
        {
            string fileName = "";
            if (image != null)
            {
                fileName = Path.Combine(_hostingEnvironment.WebRootPath, Path.GetFileName(image.FileName));
                image.CopyTo(new FileStream(fileName, FileMode.Create));

            }
            return fileName;
        }
        //------getallanswersheethavingsamequizId-----------------------------------------------------------------
        public async Task<List<AnswerSheet>> GetAllAnswerSheetHavingSameQuizID(int id)
        {
            var listOfAnswerSheet = await _context.AnswerSheets.Where(q => q.quizId.Equals(id)).AsNoTracking().ToListAsync();
            return listOfAnswerSheet;
        }
        //----------get a answersheet---------------------------------------------
        public async Task<AnswerSheet> GetAAnswerSheetInformation(AnswerSheet answerSheet)
        {
            var answerSheetInfo = await _context.AnswerSheets.Where(q => q.quizId.Equals(answerSheet.quizId) && q.userId.Equals(answerSheet.userId) && q.questionId.Equals(answerSheet.questionId)).AsNoTracking().FirstOrDefaultAsync();
            return answerSheetInfo;
        }
        //----------get a quiz -----------------------------------
        public async Task<Quiz> GetAQuizInformation(int id)
        {
            var quiz = await _context.Quizzes.Where(q => q.id.Equals(id)).AsNoTracking().SingleOrDefaultAsync();
            return quiz;
        }
        public async Task<List<Participant>> GetListOfPariticipants(int id)
        {
            var listOfParticipants = await _context.Participants.Where(q => q.quizId.Equals(id)).ToListAsync();
            return listOfParticipants;
        }
        //----------make a question list---------------------------------------------------
        public async Task<List<Question>> GetAQuestionsList(int id)
        {
            List<Question> questionslist = await _context.Questions
                     .Where(q => q.quizId.Equals(id))
                     .AsNoTracking()
                     .ToListAsync();
            return questionslist;
        }
        //------------------------------------------------------Module

        // GET: Quiz
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            //return View(await _context.Quizzes.ToListAsync());
            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
                    if (check)
                    {
                                ViewData["CurrentSort"] = sortOrder;
                                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                                ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
                                if (searchString != null)
                                {
                                        page = 1;
                                }
                                else
                                {
                                        searchString = currentFilter;
                                }
                                ViewData["CurrentFilter"] = searchString;

                                var quizContext = from s in _context.Quizzes
                                                  select s;
                                if (!String.IsNullOrEmpty(searchString))
                                {
                                        quizContext = quizContext.Where(s => s.text.Contains(searchString)
                                                               || s.id.ToString().Contains(searchString));
                                }

                                switch (sortOrder)
                                {
                                        case "name_desc":
                                            quizContext = quizContext.OrderByDescending(s => s.text);
                                            break;
                                        case "Date":
                                            quizContext = quizContext.OrderBy(s => s.id);
                                            break;
                                        case "date_desc":
                                            quizContext = quizContext.OrderByDescending(s => s.id);
                                            break;
                                        default:
                                            quizContext = quizContext.OrderBy(s => s.text);
                                            break;
                                }
                                int pageSize = 15;
                                return View(await PaginatedList<Quiz>.CreateAsync(quizContext.AsNoTracking(), page ?? 1, pageSize));
                                //return View(await _context.Quizzes.ToListAsync());
                    }
            return View("~/Views/Account/AccessDenied.cshtml");

            
        }


        //----------------------------------------------------------------------
        //get quiz name

       



        //---Why we need this savedata1 . receive answer from client side----------------------------------------------------------------
        [HttpPost]
        public async Task<JsonResult> SaveData1(string data)
        {

            AnswerSheetVM answerSheetVM = JsonConvert.DeserializeObject<AnswerSheetVM>(data);
            //chuyển đổi dữ liệu thô thành dữ liệu xài được
            var questionslength = answerSheetVM.questions.Count();
            var currentDateTime = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us"));
            List<AnswerSheet> answerSheets = new List<AnswerSheet>();
            List<int> ListOfAnswerSheetQuestionIdInDatabase = new List<int>();
            ListOfAnswerSheetQuestionIdInDatabase = await GetAListOfAnswerSheetQuestionID(answerSheetVM.questions[0]);
            ListOfAnswerSheetQuestionIdInDatabase.Sort();
            var questionListOfThisQuiz = await GetAQuestionsList(answerSheetVM.quiz.id);
            
            for (var i = 0 ; i < questionslength ; i++)
            {
                AnswerSheet answerSheet = new AnswerSheet();
                answerSheet.dateSubmitted = currentDateTime;
                answerSheet.quizId = answerSheetVM.quiz.id;
                answerSheet.questionId = answerSheetVM.questions[i].id;
                answerSheet.userId = answerSheetVM.questions[i].userId;
                answerSheet.questionType = answerSheetVM.questions[i].multiplechoice;
                answerSheet.second = answerSheetVM.cycletime[i+1];
                if (answerSheetVM.questions[i].multiplechoice)
                {
                    answerSheet.answerText = answerSheetVM.selections[i];
                    answerSheet.correctAnswer = answerSheetVM.questions[i].correctAnswer;
                    if (answerSheet.answerText == answerSheet.correctAnswer)
                    {
                        answerSheet.isCorrect = true;
                    }
                    else
                    {
                        answerSheet.isCorrect = false;
                    }
                }
                else
                {
                    answerSheet.openAnswer = answerSheetVM.selections[i];
                }
                //cho vào 1 cái list
                answerSheets.Add(answerSheet);
            }
            bool status = false;
            string message = string.Empty;
            //kiểm tra tồn tại kết quả trước khi lưu vào cơ sở dữ liệu
            var answerSheetslength = answerSheets.Count();
            for (var j = 0; j < answerSheetslength; j++)
            {
                var check = ListOfAnswerSheetQuestionIdInDatabase.Contains(answerSheets[j].questionId);
                if (check == false)
                {
                    try
                    {
                        _context.Add(answerSheets[j]);
                        _context.SaveChanges();
                        status = true;
                    }
                    catch (Exception ex)
                    {
                        status = false;
                        message = ex.Message;
                    }
                }
            }

            var current_User = await _userManager.GetUserAsync(HttpContext.User);

            var participants = await _context.Participants.Where(p => p.userId.Equals(current_User.UserName) && p.quizId == answerSheetVM.quiz.id).ToListAsync();

            if (participants.Count() == 0)
            {
                Participant newparticipant = new Participant();
                newparticipant.quizId = answerSheetVM.quiz.id;
                newparticipant.userId = current_User.UserName;
                newparticipant.alreadyDoneThisTest = true;
                try
                {
                    _context.Add(newparticipant);
                    _context.SaveChanges();
                    status = true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
            else
            {
                participants[0].alreadyDoneThisTest = true;

                try
                {
                    _context.Add(participants[0]);
                    _context.SaveChanges();
                    status = true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            
            return  Json(new
            {
                status,
                message
            });
        }
        //-----------------------------------------------------------------------------------------------------
        private async Task<List<int>> GetAListOfAnswerSheetQuestionID(questionVM questionVM)
        {
            var answerSheetsListQuestionID = await _context.AnswerSheets
                .Where(a => a.quizId.Equals(questionVM.quizId) && a.userId.Equals(questionVM.userId))
                .Select(a => a.questionId ).ToListAsync();
            return answerSheetsListQuestionID;
        }

        //----------------------------------------------------------------------

        //input: answersheet  output: bool
        public async Task<bool> CheckIfAnswerSheetExist(AnswerSheet answerSheet)
        {
            var answerSheetFromDatabase = await _context.AnswerSheets
                .Where(a => a.userId.Equals(answerSheet.userId) && a.quizId.Equals(answerSheet.quizId))
                .AsNoTracking().SingleAsync();
            if (answerSheetFromDatabase != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //----------------------------------------------------------------------
        //Clone a quiz
        [Authorize]
        public async Task<IActionResult> MakeACopyFromAQuiz(int id) {

            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {

                //--------------------------
                var quiz = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);
                if (quiz == null)
                {
                    return NotFound();
                }
                var checkOwnerOrAdmin = AccountPolicy.CheckTheOwnerOrAdminPolicy(current_User, quiz);
                // Chi co owner cua quiz hoac user co accountlevel tu 4 tro len moi co the dang nhap xem tat ca cac quiz
                if (checkOwnerOrAdmin)
                {
                                    
                                    string dt = "";
                                    dt = DateTime.Now.ToString();
                                    //Quiz quizcopy = new Quiz();

                                    //tạo 1 quiz mới rồi thêm quiz vào database
                                    var quizcopy = new Quiz
                                    {
                                        text = quiz.text + " - (copy) - " + dt,
                                        authorId = quiz.authorId,
                                        deadLine = quiz.deadLine,
                                        status = quiz.status,
                                        targetScore = quiz.targetScore,
                                        showResult = quiz.showResult,
                                        timer = quiz.timer,
                                    };
                                    _context.Add(quizcopy);
                                    await _context.SaveChangesAsync();


                                    //lay het question ra , duyet tung question rồi save vào database
                                    List<Question> questionslist = await GetAQuestionsList(id);
                                    foreach (var q in questionslist)
                                    {
                                        Question question = new Question
                                        {
                                            quizId = quizcopy.id,
                                            text = q.text,
                                            A = q.A,
                                            B = q.B,
                                            C = q.C,
                                            D = q.D,
                                            E = q.E,
                                            correct = q.correct,
                                            multipleChoice = q.multipleChoice,
                                            imageSource = q.imageSource,
                                            imageSourceA = q.imageSourceA,
                                            imageSourceB = q.imageSourceB,
                                            imageSourceC = q.imageSourceC,
                                            imageSourceD = q.imageSourceD,
                                            imageSourceE = q.imageSourceE,
                                        };
                                        _context.Add(question);
                                        await _context.SaveChangesAsync();
                                    }

                                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View("~/Views/Quiz/DeleteError.cshtml");
                }

                //---------------------------------
                
            }
            return View("~/Views/Account/AccessDenied.cshtml");
            

            
        }

        //----export result-----------------------------------------------
        
        public async Task<IActionResult> ExportResult(int id)
        {

            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //var quizmodel = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);


            var quiz = await GetAQuizInformation(id);
            //list of quizdetailviewmodel contain result
            List<QuizDetailViewModel> listOfQuizDetailViewModel = new List<QuizDetailViewModel>();
            //retreive all answersheet having same quizID
            var ListOfAnswerSheet = await GetAllAnswerSheetHavingSameQuizID(id);
            //create a list of user take part in this quiz
            List<int> listOfUserId = new List<int>();
            ViewBag.id = id;
           
                foreach (AnswerSheet a in ListOfAnswerSheet)
                {
                    var check = listOfUserId.Contains(a.userId);
                    if (check == false)
                    {
                        listOfUserId.Add(a.userId);
                    }
                }
                foreach (var userId in listOfUserId)
                {
                    //using findingresult method to make a quiz detail view model
                    QuizDetailViewModel quizDetailViewModel = FindingResultByUserId(userId, ListOfAnswerSheet, quiz);


                    var userInfo = await _context.Users.Where(u => u.Id == quizDetailViewModel.userId).AsNoTracking().SingleOrDefaultAsync();
                    quizDetailViewModel.userName = userInfo.UserName;
                    quizDetailViewModel.fullName = userInfo.fullName;
                    quizDetailViewModel.nickName = userInfo.nickName;
                    //----------------------------------------------------------
                    //check if  quiz detail view model is exist  
                    //--------------------------------------process improve the coding here
                    //List<QuizDetailViewModel> SortedList = listOfQuizDetailViewModel.OrderBy(o => o.userId).ToList();
                    //listOfQuizDetailViewModel = SortedList;

                    var check = listOfQuizDetailViewModel.Contains(quizDetailViewModel);
                    // if list is empty or quiz not exist in list , then add quiz detail to list
                    if (check == false)
                    {
                        listOfQuizDetailViewModel.Add(quizDetailViewModel);
                    }

                }

            //------------------------------------------------------
            //tao mot memory stream
            var memory = new MemoryStream();
            DateTime datetime = DateTime.Now;
            var quiznametemp = Regex.Replace(quiz.text, "[^0-9a-zA-Z]+", "");
            string name = quiznametemp + "FinalResult"+ ".xlsx";
            string result = string.Concat(name.Where(c => !char.IsWhiteSpace(c)));
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            //---------------
            string folderName = "report";

            string newPath = Path.Combine(sWebRootFolder, folderName);

            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            //-----------
            string sFileName = @result;
            //FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            using (var fs = new FileStream(Path.Combine(newPath, sFileName), FileMode.Create, FileAccess.Write))
            {

                //tao workbook moi
                IWorkbook workbook;
                workbook = new XSSFWorkbook();

                //tao worksheet moi
                ISheet excelSheet = workbook.CreateSheet("Result");
                //duyet list va dua du lieu vao work boook
                var countRawReport = listOfQuizDetailViewModel.Count();
                IRow row = excelSheet.CreateRow(0);

                row.CreateCell(0).SetCellValue("ID No.");
                row.CreateCell(1).SetCellValue("Nick Name");
                row.CreateCell(2).SetCellValue("Full Name");
                row.CreateCell(3).SetCellValue("DateTime Submitted");
                row.CreateCell(4).SetCellValue("Score(%)");
                row.CreateCell(5).SetCellValue("Cycle Time");
                row.CreateCell(6).SetCellValue("Result");


                for (var i = 0; i < countRawReport; i++)
                {
                    row = excelSheet.CreateRow(i + 1);

                    row.CreateCell(0).SetCellValue(listOfQuizDetailViewModel[i].userName);
                    row.CreateCell(1).SetCellValue(listOfQuizDetailViewModel[i].nickName);
                    row.CreateCell(2).SetCellValue(listOfQuizDetailViewModel[i].fullName);
                    row.CreateCell(3).SetCellValue(listOfQuizDetailViewModel[i].submittionDateTime);
                    row.CreateCell(4).SetCellValue(listOfQuizDetailViewModel[i].score);
                    row.CreateCell(5).SetCellValue(listOfQuizDetailViewModel[i].CycleTime);
                    row.CreateCell(6).SetCellValue(listOfQuizDetailViewModel[i].result);

                }

                //ghi workboot này vào memory stream
                workbook.Write(fs);
            }


            //tra ve view ket qua
            using (var stream = new FileStream(Path.Combine(newPath, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);


            }
            var deletepath = Path.Combine(newPath, sFileName);
            if (System.IO.File.Exists(@deletepath))
            {
                // Use a try block to catch IOExceptions, to
                // handle the case of the file already being
                // opened by another process.
                try
                {
                    System.IO.File.Delete(@deletepath);
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            memory.Position = 0;
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
            //--------------------------------------------------------------------------------------------------

        }
        //---Export data excel file------------------------------------------------------
        public async Task<IActionResult> ExportAllData(int id)
        {
            //nhan vao id va tim kiem quiz
            Quiz quiz = await GetAQuizInformation(id);
            // co quiz thi di tim kiem list answer sheet
            var ListOfAnswerSheet = await GetAllAnswerSheetHavingSameQuizID(id);
            //list userID
            var listOfUserId = new List<int>();
            // list user
            var listOfUser = new List<UserInfo>();
            foreach (AnswerSheet a in ListOfAnswerSheet)
            {
                var check = listOfUserId.Contains(a.userId);
                if (check == false)
                {
                    listOfUserId.Add(a.userId);
                    ApplicationUser user = await _userManager.Users.Where(u => u.Id == a.userId).FirstAsync();
                    var userInfo = new UserInfo();
                    userInfo.id = a.userId;
                    userInfo.userId = user.UserName;
                    userInfo.userFullName = user.fullName;
                    listOfUser.Add(userInfo);
                }
            }
            //list user
            
            //list cau hoi
            var questionList = await GetAQuestionsList(id);

            // de du lieu vao doi tuong view model mong muon
           
            var rawReport = new List<RawReport>();
            // dua doi tuong vao mot list
            foreach (var b in ListOfAnswerSheet)
            {
                var temp = new RawReport();
                var userInfoTemp = listOfUser.Where(i => i.id == b.userId).FirstOrDefault();
                temp.userId = userInfoTemp.userId;
                //var userNameTemp = listOfUser.Where(i => i.id == b.userId).FirstOrDefault();
                temp.userName = userInfoTemp.userFullName;
                var  questionTemp = questionList.Where(q => q.id == b.questionId).FirstOrDefault();
                temp.question = questionTemp.text;

                if(questionTemp.multipleChoice == true)
                {
                    temp.questionType = "answer choice";
                }
                else
                {
                    temp.questionType = "open question";
                }
                
                if (b.answerText != null)
                {
                    temp.choice = b.answerText;
                }
                else
                {
                    temp.choice = "";
                }
                if (b.correctAnswer != null)
                {
                    temp.correctanswer = b.correctAnswer;
                }
                else
                {
                    temp.correctanswer = "";
                }
                
                temp.check = b.isCorrect;
                if(b.openAnswer != null)
                {
                    temp.openAnswer = b.openAnswer;
                }
                else
                {
                    temp.openAnswer = "";
                }
                temp.time = b.second;
                //add model to list of raw report
                rawReport.Add(temp);
            }

            //tao mot memory stream
            var memory = new MemoryStream();
            DateTime datetime = DateTime.Now;
            var quiznametemp = Regex.Replace(quiz.text, "[^0-9a-zA-Z]+", "");
            string name = quiznametemp + "AllData"+".xlsx" ;
            string result = string.Concat(name.Where(c => !char.IsWhiteSpace(c)));
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            //---------------
            string folderName = "report";
            
            string newPath = Path.Combine(sWebRootFolder, folderName);
            
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            //-----------
            string sFileName = @result;
            //FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            using (var fs = new FileStream(Path.Combine(newPath, sFileName), FileMode.Create, FileAccess.Write))
            {

                //tao workbook moi
                IWorkbook workbook;
                workbook = new XSSFWorkbook();

                //tao worksheet moi
                ISheet excelSheet = workbook.CreateSheet("Result");
                //duyet list va dua du lieu vao work boook
                var countRawReport = rawReport.Count();
                IRow row = excelSheet.CreateRow(0);

                row.CreateCell(0).SetCellValue("UserID");
                row.CreateCell(1).SetCellValue("Full Name");
                row.CreateCell(2).SetCellValue("Question");
                row.CreateCell(3).SetCellValue("Question Type");
                row.CreateCell(4).SetCellValue("Choice");
                row.CreateCell(5).SetCellValue("Answer");
                row.CreateCell(6).SetCellValue("Check");
                row.CreateCell(7).SetCellValue("Open Answer");
                row.CreateCell(8).SetCellValue("Cycle time");


                for (var i = 0; i < countRawReport; i++)
                {
                    row = excelSheet.CreateRow(i + 1);

                    row.CreateCell(0).SetCellValue(rawReport[i].userId);
                    row.CreateCell(1).SetCellValue(rawReport[i].userName);
                    row.CreateCell(2).SetCellValue(rawReport[i].question);
                    row.CreateCell(3).SetCellValue(rawReport[i].questionType);
                    row.CreateCell(4).SetCellValue(rawReport[i].choice);
                    row.CreateCell(5).SetCellValue(rawReport[i].correctanswer);
                    row.CreateCell(6).SetCellValue(rawReport[i].check);
                    row.CreateCell(7).SetCellValue(rawReport[i].openAnswer);
                    row.CreateCell(8).SetCellValue(rawReport[i].time);

                }

                //ghi workboot này vào memory stream
                workbook.Write(fs);
            }


            //tra ve view ket qua
            using (var stream = new FileStream(Path.Combine(newPath, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);

                
            }
            var deletepath = Path.Combine(newPath, sFileName);
            if (System.IO.File.Exists(@deletepath))
            {
                // Use a try block to catch IOExceptions, to
                // handle the case of the file already being
                // opened by another process.
                try
                {
                    System.IO.File.Delete(@deletepath);
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            memory.Position = 0;
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
           
        }

        //Get :Quiz/QuizResult intput: quiz id   Output : result
        [Authorize]
        public async Task<IActionResult> QuizResult(int id)
        {
            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var checkAccountLevelPolicy = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (checkAccountLevelPolicy)
            {

                        //--------------------------
                        var quiz = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);
                        if (quiz == null)
                        {
                            return NotFound();
                        }
                        var checkOwnerOrAdmin = AccountPolicy.CheckTheOwnerOrAdminPolicy(current_User, quiz);
                        // Chi co owner cua quiz hoac user co accountlevel tu 4 tro len moi co the dang nhap xem tat ca cac quiz
                        if (checkOwnerOrAdmin)
                        {

                                    
                                            //list of quizdetailviewmodel contain result
                                            List<QuizDetailViewModel> listOfQuizDetailViewModel = new List<QuizDetailViewModel>();
                                            //retreive all answersheet having same quizID
                                            var ListOfAnswerSheet = await GetAllAnswerSheetHavingSameQuizID(id);
                                            //create a list of user take part in this quiz
                                            List<int> listOfUserId = new List<int>();
                                            ViewBag.id = id;
                                            foreach (AnswerSheet a in ListOfAnswerSheet)
                                            {
                                                var check = listOfUserId.Contains(a.userId);
                                                if (check == false)
                                                {
                                                    listOfUserId.Add(a.userId);
                                                }
                                            }
                                            foreach (var userId in listOfUserId)
                                            {
                                                //using findingresult method to make a quiz detail view model
                                                QuizDetailViewModel quizDetailViewModel = FindingResultByUserId(userId, ListOfAnswerSheet, quiz);


                                                var userInfo = await _context.Users.Where(u => u.Id == quizDetailViewModel.userId).AsNoTracking().SingleOrDefaultAsync();
                                                quizDetailViewModel.userName = userInfo.UserName;
                                                quizDetailViewModel.fullName = userInfo.fullName;
                                                quizDetailViewModel.nickName = userInfo.nickName;
                                                //----------------------------------------------------------
                                                //check if  quiz detail view model is exist  
                                                //--------------------------------------process improve the coding here
                                                //List<QuizDetailViewModel> SortedList = listOfQuizDetailViewModel.OrderBy(o => o.userId).ToList();
                                                //listOfQuizDetailViewModel = SortedList;

                                                var check = listOfQuizDetailViewModel.Contains(quizDetailViewModel);
                                                // if list is empty or quiz not exist in list , then add quiz detail to list
                                                if (check == false)
                                                {
                                                    listOfQuizDetailViewModel.Add(quizDetailViewModel);
                                                }

                                            }
                                            return View(listOfQuizDetailViewModel);

                        }
                        else
                        {
                            return View("~/Views/Quiz/DeleteError.cshtml");
                        }

                        //---------------------------------

                //var quizmodel = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);


                
            }
            return View("~/Views/Account/AccessDenied.cshtml");

           
            
        }

        //[Authorize]
        //public async Task<JsonObjectAttribute> QuizResult2 (int id)
        //{

        //    var current_User = await _userManager.GetUserAsync(HttpContext.User);
        //    //var quizmodel = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);


        //    var quiz = await GetAQuizInformation(id);
        //    //list of quizdetailviewmodel contain result
        //    List<QuizDetailViewModel> listOfQuizDetailViewModel = new List<QuizDetailViewModel>();
        //    //retreive all answersheet having same quizID
        //    var ListOfAnswerSheet = await GetAllAnswerSheetHavingSameQuizID(id);
        //    //create a list of user take part in this quiz
        //    List<int> listOfUserId = new List<int>();
        //    if (quiz.authorId == current_User.UserName)
        //    {
        //        foreach (AnswerSheet a in ListOfAnswerSheet)
        //        {
        //            var check = listOfUserId.Contains(a.userId);
        //            if (check == false)
        //            {
        //                listOfUserId.Add(a.userId);
        //            }
        //        }
        //    }


        //}

        //--------------------------------------------------------------------
        public string ConvertSecondsToMinute(int cycletime)
        {
            var minutes = cycletime / 60;
            var seconds = cycletime % 60;
            var time = minutes.ToString() + ":" + seconds.ToString();
            return time;
        }
        //----------------------------------------------------------------------

        // recieve a answersheet  , then count all answersheet having same quizid and userid
        // then provide the result as QuizDetailViewModel
        //input : 1 answersheet  output : true/false
        public QuizDetailViewModel FindingResultByUserId(int userId,List<AnswerSheet> listOfAnswerSheet,Quiz quiz)
        {
            int cycletime = 0;
            double score = 0;
            int numberOfQuestion = 0;
            int numberOfCorrectAnswer = 0;
            var date = "";
            var quizDetailViewModel = new QuizDetailViewModel();
            
            foreach (AnswerSheet a in listOfAnswerSheet)
            {
                if (a.userId == userId)
                {
                    date = a.dateSubmitted;
                    if (a.questionType == true)
                    {
                        numberOfQuestion += 1;
                        cycletime += Int32.Parse(a.second);
                        
                        if (a.isCorrect == true)
                        {
                            numberOfCorrectAnswer += 1;
                        }
                    }
                }
                

            }
            quizDetailViewModel.userId = userId;
            quizDetailViewModel.submittionDateTime = date;
            quizDetailViewModel.CycleTime = ConvertSecondsToMinute(cycletime);
            if(numberOfQuestion != 0)
            {
                score = (numberOfCorrectAnswer * 100) / numberOfQuestion;
            }
            else
            {
                score = 0;
            }
          
            quizDetailViewModel.score = Math.Round(score,2);
            if(quiz.targetScore != null)
            {
                if(score != 0)
                {
                    if (quizDetailViewModel.score >= Double.Parse(quiz.targetScore))
                    {
                        quizDetailViewModel.result = "Passed";
                    }
                    else
                    {
                        quizDetailViewModel.result = "Fail";
                    }
                }
                else
                {
                    quizDetailViewModel.result = "Not available";
                }
            }
            else
            {
                quizDetailViewModel.result = "Not available";
            }
            
            return  quizDetailViewModel;
        }

        //----------------------------------------------------------------------


        //----------------------------------------------------------------------

        //intput: list of quizdetailviewmodel  output: bool
        //public bool CheckQuizDetailViewModelExist(List<QuizDetailViewModel> listOfQuizDetailVM, QuizDetailViewModel quizDetailViewModel)
        //{
        //    bool check = false;
        //    for (int i = 0; i < listOfQuizDetailVM.Count; i++)
        //    {
        //        if (quizDetailViewModel.userId == listOfQuizDetailVM[i].userId)
        //        {
        //            check = true;
        //        }
        //    }
        //    return check;
        //}

        //----------------------------------------------------------------------

        // GET: Quiz/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int id)
        {

            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var checkAccountLevelPolicy = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (checkAccountLevelPolicy)
            {
                            //--------------------------
                            var quiz = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);
                            if (quiz == null)
                            {
                                return NotFound();
                            }
                            var checkOwnerOrAdmin = AccountPolicy.CheckTheOwnerOrAdminPolicy(current_User, quiz);
                            // Chi co owner cua quiz hoac user co accountlevel tu 4 tro len moi co the dang nhap xem tat ca cac quiz
                            if (checkOwnerOrAdmin)
                            {


                                    var quizmodel = await _context.Quizzes
                                       .Include(q => q.questions)
                                       .SingleOrDefaultAsync(m => m.id == id);                
                                    return View(quizmodel);

                            }
                            else
                            {
                                return View("~/Views/Quiz/DeleteError.cshtml");
                            }

                            //---------------------------------
               
            }
            return View("~/Views/Account/AccessDenied.cshtml");
        }

        //----------------------------------------------------------------------

        // GET: Quiz/Participant/5
        [Authorize]
        public async Task<IActionResult> ParticipantsList(int id)
        {

            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {


                        //--------------------------
                        var quiz = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);
                        if (quiz == null)
                        {
                            return NotFound();
                        }
                        var checkOwnerOrAdmin = AccountPolicy.CheckTheOwnerOrAdminPolicy(current_User, quiz);
                        // Chi co owner cua quiz hoac user co accountlevel tu 4 tro len moi co the dang nhap xem tat ca cac quiz
                        if (checkOwnerOrAdmin)
                        {


                                        var quizmodel = await _context.Quizzes.Include(q => q.participants).SingleOrDefaultAsync(m => m.id == id);
                                        return View(quizmodel);

                        }
                        else
                        {
                            return View("~/Views/Quiz/DeleteError.cshtml");
                        }

                        //---------------------------------
                
            }
            return View("~/Views/Account/AccessDenied.cshtml");
        }

        //----------------------------------------------------------------------

        // GET: Quiz/Create
        [Authorize]
        public async Task<IActionResult> Create()
        {
            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                
                ViewData["authorId"] = current_User.UserName;
                return View();
            }
            return View("~/Views/Account/AccessDenied.cshtml");
            
        }

        //----------------------------------------------------------------------

        // POST: Quiz/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("id,text,authorId,deadLine,targetScore,timer,status,showResult,ramdomizeOrderOfAnswer,ramdomizeOrderOfQuestion")] Quiz quiz)
        {
            //quiz.dateCreated = DateTime.Now;
            // nếu model không hợp lệ thì return lại trang view partial
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            quiz.authorId = current_User.UserName;
            if (ModelState.IsValid)
            {
                //nếu ok thì save 
                _context.Add(quiz);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            }

            return View(quiz);

        }


        //----------------------------------------------------------------------

        // GET: Quiz/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var checkAccountLevelPolicy = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (checkAccountLevelPolicy)
            {

                    var quiz = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);
                    if (quiz == null)
                    {
                        return NotFound();
                    }
                    var checkOwnerOrAdmin = AccountPolicy.CheckTheOwnerOrAdminPolicy(current_User, quiz);
                    // Chi co owner cua quiz hoac user co accountlevel tu 4 tro len moi co the dang nhap xem tat ca cac quiz
                    if (checkOwnerOrAdmin)
                    {
                                return View(quiz);
                    }
                    else
                    {
                                return View("~/Views/Quiz/DeleteError.cshtml");
                    }

            }
            return View("~/Views/Account/AccessDenied.cshtml");

           
        }

        //----------------------------------------------------------------------

        // POST: Quiz/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("id,text,authorId,deadLine,targetScore,timer,status,showResult,ramdomizeOrderOfAnswer,ramdomizeOrderOfQuestion")] Quiz quiz)
        {
          
            if (id != quiz.id)
            {
                return NotFound();
            }
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //set lai user id o day help security , however
            //quiz.authorId = current_User.UserName;
            //var quizInformation = await _context.Quizzes.Where(q => q.id.Equals(id)).FirstAsync();

            
            //if (quiz.authorId != quizInformation.authorId)
            //    {
            //            quiz.authorId = quizInformation.authorId;
            //    }

            if (ModelState.IsValid)
            {
                            try
                            {
                                _context.Update(quiz);
                                await _context.SaveChangesAsync();
                            }
                            catch (DbUpdateConcurrencyException)
                            {
                                if (!QuizExists(quiz.id))
                                {
                                    return NotFound();
                                }
                                else
                                {
                                    throw;
                                }
                            }
                            return RedirectToAction(nameof(Index));
            }

            return View(quiz);
        }

        //----------------------------------------------------------------------

        // GET: Quiz/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                        var quiz = await _context.Quizzes.SingleOrDefaultAsync(m => m.id == id);
                        if (quiz == null)
                        {
                                    return NotFound();
                        }
                        var checkOwnerOrAdmin = AccountPolicy.CheckTheOwnerOrAdminPolicy(current_User, quiz);
                        // Chi co owner cua quiz hoac user co accountlevel tu 4 tro len moi co the dang nhap xem tat ca cac quiz
                        if (checkOwnerOrAdmin)
                        {
                    
                                    if (quiz == null)
                                    {
                                        return NotFound();
                                    }

                                    return View(quiz);
                        }
                        else
                        {
                                    return View("~/Views/Quiz/DeleteError.cshtml");
                        }
                        //-----------
               
             }
            return View("~/Views/Account/AccessDenied.cshtml");

            
        }

        //----------------------------------------------------------------------

        // POST: Quiz/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
            var quizmodel = await GetAQuizInformation(id);
            if (quizmodel == null)
            {
                return NotFound();
            }
            _context.Quizzes.Remove(quizmodel);
            await _context.SaveChangesAsync();
          
            return RedirectToAction(nameof(Index));
        }

        //----------------------------------------------------------------------
        [Authorize]
        public async Task<IActionResult> SelectQuiz(string sortOrder, string searchString /*, int? page*/, string currentFilter)
        {

            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            //ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
            ViewData["CurrentFilter"] = searchString;

            var quizContext = from s in _context.Quizzes
                              select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                //quizContext = quizContext.Where(s => s.text.Contains(searchString)
                //                       || s.id.ToString().Contains(searchString));
                quizContext = quizContext.Where(s=>s.id.ToString().Equals(searchString));
            }
            //var quizContext = _context.Quizzes.Include(q => q.User).ToListAsync;
            switch (sortOrder)
            {
                case "name_desc":
                    quizContext = quizContext.OrderByDescending(s => s.id);
                    break;
                //case "date":
                //    quizContext = quizContext.OrderBy(s => s.text);
                //    break;
                //case "date_desc":
                //    quizContext = quizContext.OrderByDescending(s => s.text);
                //    break;
                default:
                    quizContext = quizContext.OrderBy(s => s.id);
                    break;
            }
            //current user
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //Retreive all quiz that user already done
            var listOfQuizAlreadyDoneByCurrentUser = await _context.Participants
                .Where(p => p.userId.Equals(current_User.UserName) && p.alreadyDoneThisTest == true)
                .Select(item => item.quiz).ToListAsync();

            var searchResult = await quizContext.AsNoTracking().ToListAsync();

            //----------
            //if(listOfQuizAlreadyDoneByCurrentUser != null)
            //{
            //    foreach (var q in listOfQuizAlreadyDoneByCurrentUser)
            //    {
            //        searchResult.Remove(q);
            //    }
            //}
            //lam cach ngu hoc trước
            var blackList = new List<int>();
            for(var i = 0; i < listOfQuizAlreadyDoneByCurrentUser.Count() ;i++)
            {
                for (var j = 0; j < searchResult.Count(); j++)
                {
                    if(searchResult[j].id.Equals(listOfQuizAlreadyDoneByCurrentUser[i].id))
                    {
                        blackList.Add(j);
                    }
                }
            }

            for (var i = blackList.Count() ; i > 0; i--)
            {
                if(blackList[i-1] <= searchResult.Count())
                {
                    searchResult.RemoveAt(blackList[i-1]);
                }
                
            }

            //----------
            return View(searchResult);
        }


        //----------------------------------------------------------------------

        // handle select quiz 1
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> SelectQuiz(/*QuizViewModel quiz*/ int id)
        {
            Quiz quizSelected = await GetAQuizInformation(id);
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            DateTime dt = new DateTime();
            quizSelected.deadLine += " 11:59:00 PM";
            dt = Convert.ToDateTime(quizSelected.deadLine);
            //dt = DateTime.ParseExact(quizSelected.deadLine, "dd/MM/yyyy", null);
            var participants = await GetListOfPariticipants(id);
            var listOfUserId = new List<string>();
            foreach(Participant p in participants)
            {
                var check = listOfUserId.Contains(p.userId);
                if (check == false)
                {
                    listOfUserId.Add(p.userId);
                }
            }

            if (quizSelected != null)
            {

                if (dt < DateTime.Now || quizSelected.status == false)
                {

                    return View("~/Views/Quiz/Deadline.cshtml", quizSelected);
                }
                else
                {
                    HttpContext.Session.Set<Quiz>(SelectedQuiz, quizSelected);
                    return RedirectToAction("QuizTest");
                }
                //if (listOfUserId.Count == 0)
                //{

                //    if (dt < DateTime.Now || quizSelected.status == false)
                //    {

                //        return View("~/Views/Quiz/Deadline.cshtml", quizSelected);
                //    }
                //    else
                //    {
                //        HttpContext.Session.Set<Quiz>(SelectedQuiz, quizSelected);
                //        return RedirectToAction("QuizTest");
                //    }
                //}
                //else
                //{
                //    var check = listOfUserId.Contains(current_User.UserName);
                //    if(check == true)
                //    {
                //        if (dt < DateTime.Now || quizSelected.status == false)
                //        {

                //            return View("~/Views/Quiz/Deadline.cshtml", quizSelected);
                //        }
                //        else
                //        {
                //            HttpContext.Session.Set<Quiz>(SelectedQuiz, quizSelected);
                //            return RedirectToAction("QuizTest");
                //        }
                //    }
                //    else
                //    {
                //        return View("~/Views/Quiz/NotForYou.cshtml", quizSelected);
                //    }
                //}
                

            }
            return View();
        }

        //----------------------------------------------------------------------

        //[HttpGet]
        [Authorize]
        public async Task<IActionResult> QuizTest()
        {
            Quiz quizSelected = HttpContext.Session.Get<Quiz>(SelectedQuiz) /*as QuizViewModel*/;
            List<Question> questionslist = null;
            //ViewData["quizName"] = quizSelected.quizName;


            if (quizSelected != null)
            {
                questionslist = await GetAQuestionsList(quizSelected.id);
            }
            else
            {
                return View("~/Views/Quiz/SelectQuiz.cshtml");
            }


            List<questionVM> questions = new List<questionVM>();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //if (current_User == null)
            //{
            //    return View("~/Views/Account/Login.cshtml");
            //}
            for (var i = 0; i < questionslist.Count; i++)
            {
                questionVM questionVM = new questionVM();
                questionVM.id = questionslist[i].id;
                questionVM.quizId = questionslist[i].quizId;
                questionVM.userId = current_User.Id;
                questionVM.questiontext = questionslist[i].text;
                questionVM.QuestionImage = questionslist[i].imageSource;
                questionVM.multiplechoice = questionslist[i].multipleChoice;
                //    questionVM.correct = new correctVM();
                //questionVM.correctAnswer = ;
                questionVM.answers = new Answer[5];
                if (questionslist[i].multipleChoice == true)
                {
                    //--------------------------------------
                    if(questionslist[i].A != null || questionslist[i].imageSourceA != null)
                    {
                        questionVM.answers[0] = new Answer { letter = "A", answer = questionslist[i].A, imagesource = questionslist[i].imageSourceA };
                    }
                    else
                    {
                        questionVM.answers[0] = null;
                    }
                    //-------------------------------
                    if (questionslist[i].B != null || questionslist[i].imageSourceB != null)
                    {
                        questionVM.answers[1] = new Answer { letter = "B", answer = questionslist[i].B, imagesource = questionslist[i].imageSourceB };
                    }
                    else
                    {
                        questionVM.answers[1] = null;
                    }
                    //-------------------------------
                    if (questionslist[i].C != null || questionslist[i].imageSourceC != null)
                    {
                        questionVM.answers[2] = new Answer { letter = "C", answer = questionslist[i].C, imagesource = questionslist[i].imageSourceC };
                    }
                    else
                    {
                        questionVM.answers[2] = null;
                    }
                    //---------------------------------------------------
                    if (questionslist[i].D != null || questionslist[i].imageSourceD != null)
                    {
                        questionVM.answers[3] = new Answer { letter = "D", answer = questionslist[i].D, imagesource = questionslist[i].imageSourceD };
                    }
                    else
                    {
                        questionVM.answers[3] = null;
                    }
                    //----------------------------------------------------------------------------
                    if (questionslist[i].E != null || questionslist[i].imageSourceE != null)
                    {
                        questionVM.answers[4] = new Answer { letter = "E", answer = questionslist[i].E, imagesource = questionslist[i].imageSourceE };
                    }
                    else
                    {
                        questionVM.answers[4] = null;
                    }
                    
                    

                    switch (questionslist[i].correct.ToString())
                    {
                        case "A":
                            //questionVM.correct.index = 0;
                            questionVM.correctAnswer = "A";
                            break;
                        case "B":
                            //questionVM.correct.index = 1;
                            questionVM.correctAnswer = "B";
                            break;
                        case "C":
                            // questionVM.correct.index = 2;
                            questionVM.correctAnswer = "C";
                            break;
                        case "D":
                            //questionVM.correct.index = 3;
                            questionVM.correctAnswer = "D";
                            break;
                        case "E":
                            //questionVM.correct.index = 3;
                            questionVM.correctAnswer = "E";
                            break;
                        default:
                            // questionVM.correct.index = 4;
                            questionVM.correctAnswer = null;
                            break;
                    }
                    //shuffle the answer
                    if (quizSelected.ramdomizeOrderOfAnswer)
                    {
                        Shuffle(questionVM.answers);
                    }
                    

                }
                questions.Add(questionVM);
            }
            //convert list to array
            questionVM[] array = new questionVM[questions.Count()];
            array = questions.ToArray(); // fill the array
            //shuffle the question
            if (quizSelected.ramdomizeOrderOfQuestion)
            {
                Shuffle(array);
            }
            questions = array.ToList();
            DataVM dataVM = new DataVM();
            dataVM.questions = questions;
            dataVM.quizsetting = quizSelected;
            return View(dataVM);
        }
        //----Ramdomizeoderofanswer----------------------------------------------------------------------------
        public static Random _random = new Random();
        static void Shuffle<T>(T[] array)
        {
            int n = array.Length;
            for (int i = 0; i < n; i++)
            {
                // Use Next on random instance with an argument.
                // ... The argument is an exclusive bound.
                //     So we will not go past the end of the array.
                int r = i + _random.Next(n - i);
                T t = array[r];
                array[r] = array[i];
                array[i] = t;
            }
        }
        //----------------------------------------------------------------------


        //POST handle quiz test
        //[HttpPost]
        //[Authorize]
        //public async Task<IActionResult> QuizTest(List<QuizAnswersViewModel> resultQuiz /*QuizAnswersViewModel resultQuiz*/)
        //{
            
        //    List<QuizAnswersViewModel> finalResultQuiz = new List<QuizAnswersViewModel>();
        //    var currentUser = HttpContext.User;
        //    foreach (QuizAnswersViewModel answser in resultQuiz)
        //    {
        //        if ( await FindingQuestionType(answser.questionId) == true)
        //        {
        //            //defind a answer sheet model                
        //            AnswerSheet answerSheet = new AnswerSheet();
        //            QuizAnswersViewModel result = new QuizAnswersViewModel();
        //            
        //            //result.choiceText = await FindingCorrectAnswer(answser.questionId);
        //            answerSheet.feedBack = answser.feedBack;
        //            //answerSheet.actualChoiceId = answser.choiceId;
        //            answerSheet.questionId = answser.questionId;
        //            //answerSheet.quizId = await FindingQuizId(answser.questionId);
        //            answerSheet.userId = 4;
        //            //answerSheet.expectChoiceId = await FindingCorrectChoiceId(answser.questionId);
        //            answerSheet.Iscorrect = result.IsCorrect;
        //            answerSheet.dateSubmitted = DateTime.Now;
        //            if (await CheckAnswerSheetExistThenNotSave(answerSheet) == false)
        //            {
        //                SaveAnswerSheet(answerSheet);
        //            }

        //            finalResultQuiz.Add(result);
        //        }

        //        else
        //        {
        //            AnswerSheet answerSheet = new AnswerSheet();
        //            QuizAnswersViewModel result = new QuizAnswersViewModel();
        //            result = await _context.Questions.Where(a => a.id == answser.questionId).Select(a => new QuizAnswersViewModel
        //            {
        //                questionId = a.id,
        //                //   AnswerOfQuestion = a.AnswerText,
        //                questionType = "fillingtext"

        //            }).FirstOrDefaultAsync();
        //            //result.choiceText = await FindingCorrectAnswer(answser.questionId);
        //            answerSheet.feedBack = answser.feedBack;
        //            //answerSheet.actualChoiceId = answser.choiceId;
        //            answerSheet.questionId = answser.questionId;
        //            //answerSheet.quizId = await FindingQuizId(answser.questionId);
        //            answerSheet.userId = 4;
        //            //answerSheet.expectChoiceId = await FindingCorrectChoiceId(answser.questionId);
        //            //answerSheet.IsCorrect = result.IsCorrect;
        //            answerSheet.dateSubmitted = DateTime.Now;
        //            if (await CheckAnswerSheetExistThenNotSave(answerSheet) == false)
        //            {
        //                SaveAnswerSheet(answerSheet);
        //            }

        //            finalResultQuiz.Add(result);

        //        }

        //    }
        //    return Json(new { result = finalResultQuiz }/*, JsonRequestBehavior.AllowGet*/);

        //}



        //----------------------------------------------------------------------


        //save the answer sheet to database
        [HttpPost]
        public void SaveAnswerSheet ([Bind("Id,actualChoiceId,questionId,userId,quizId,expectChoiceText,IsCorrect,dateTimeSubmittion,feedBack")]AnswerSheet answerSheet)
        {
            if (ModelState.IsValid)
            {
                _context.Add(answerSheet);
                 _context.SaveChangesAsync();                
            }
        }


        //----------------------------------------------------------------------


        // check if answersheet is exist then not save
        public async Task<bool> CheckAnswerSheetExistThenNotSave (AnswerSheet answerSheet)
        {
                AnswerSheet answerSheetcontext = await _context.AnswerSheets
                .Where(i => i.quizId.Equals(answerSheet.quizId) && i.questionId.Equals(answerSheet.questionId) && i.userId.Equals(answerSheet.userId))
                .Select(i => new AnswerSheet
                {
                    questionId = i.questionId,
                    quizId = i.quizId,
                    userId = i.userId,

                }).AsNoTracking().FirstOrDefaultAsync();

            if (answerSheetcontext != null)
            { return true; }
            else
            { return false; }

        }



        //----------------------------------------------------------------------



        //----------------------------------------------------------------------


        //this for create question button in quiz index.cshtml page to call index method 
        //in question controller
        public IActionResult RedirectQuestion (int id)
        {
            return View("~/Views/Question/Index.cshtml",id);
        }


        //----------------------------------------------------------------------
        //-----------------------------------------------------------------------------


        private static List<QuizDetailViewModel> MergeSort(List<QuizDetailViewModel> unsorted)
        {
            if (unsorted.Count <= 1)
                return unsorted;

            List<QuizDetailViewModel> left = new List<QuizDetailViewModel>();
            List<QuizDetailViewModel> right = new List<QuizDetailViewModel>();

            int middle = unsorted.Count / 2;
            for (int i = 0; i < middle; i++)  //Dividing the unsorted list
            {
                left.Add(unsorted[i]);
            }
            for (int i = middle; i < unsorted.Count; i++)
            {
                right.Add(unsorted[i]);
            }

            left = MergeSort(left);
            right = MergeSort(right);
            return Merge(left, right);
        }

        //----------------------------------------------------------------------

        private static List<QuizDetailViewModel> Merge(List<QuizDetailViewModel> left, List<QuizDetailViewModel> right)
        {
            List<QuizDetailViewModel> result = new List<QuizDetailViewModel>();

            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First().score <= right.First().score)  //Comparing First two elements to see which is smaller
                    {
                        result.Add(left.First());
                        left.Remove(left.First());      //Rest of the list minus the first element
                    }
                    else
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                    }
                }
                else if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                }
                else if (right.Count > 0)
                {
                    result.Add(right.First());

                    right.Remove(right.First());
                }
            }
            return result;
        }

        //----------------------------------------------------------------------

        private bool QuizExists(int id)
        {
            return _context.Quizzes.Any(e => e.id == id);
        }
    }
}
