﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using qa8.Data;
using qa8.Models;
using qa8.Repository;

namespace qa8.Controllers
{
    [Authorize]
    public class ParticipantController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ParticipantController(
            ApplicationDbContext context
            , UserManager<ApplicationUser> userManager, 
            IHostingEnvironment hostingEnvironment
            )
        {
            _context = context;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: Participant
        [Authorize]
        public async Task<IActionResult> Index()
        {
            //// khu vuc danh cho cap 2
            var controllerPolicy = 2;

            var AccountPolicy = new AccountPolicy();
            var current_User = await _userManager.GetUserAsync(HttpContext.User);
            //check
            var check = AccountPolicy.CheckAccountLevelVsControllerPolicy(current_User, controllerPolicy);
            if (check)
            {
                var applicationDbContext = _context.Participants.Include(p => p.quiz);
                return View(await applicationDbContext.ToListAsync());
            }
            return View("~/Views/Account/AccessDenied.cshtml");
            
        }

        // GET: Participant/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participant = await _context.Participants
                .Include(p => p.quiz)
                .SingleOrDefaultAsync(m => m.id == id);
            if (participant == null)
            {
                return NotFound();
            }
            ViewData["quizId"] = participant.quizId;
            return View(participant);
        }

        // GET: Participant/Create
        [Authorize]
        public async Task<IActionResult> Create(int quizId)
        {
            Quiz quiz = await _context.Quizzes.Where(p => p.id == quizId).AsNoTracking().SingleAsync();

            ViewData["quizId"] = quiz.id;
            //ViewData["quizId"] = new SelectList(_context.Quizzes, "id", "id");
            ViewData["text"] = quiz.text;
            var s = ViewData["quizId"];
            return  View();
        }

        // POST: Participant/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("id,quizId,userId")] Participant participant)
        {
            if (ModelState.IsValid)
            {
                _context.Add(participant);
                await _context.SaveChangesAsync();
                //return RedirectToAction(nameof(Index));
                return RedirectToAction("ParticipantsList", "Quiz", new { id = participant.quizId });
            }
            //ViewData["quizId"] = new SelectList(_context.Quizzes, "id", "id", participant.quizId);
            return View(participant);
        }

        // GET: Participant/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participant = await _context.Participants.SingleOrDefaultAsync(m => m.id == id);
            if (participant == null)
            {
                return NotFound();
            }
            //ViewData["quizId"] = new SelectList(_context.Quizzes, "id", "id", participant.quizId);
            ViewData["quizId"] = participant.quizId;
            return View(participant);
        }

        // POST: Participant/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("id,quizId,userId")] Participant participant)
        {
            if (id != participant.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(participant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ParticipantExists(participant.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                //return RedirectToAction(nameof(Index));
                return RedirectToAction("ParticipantsList", "Quiz", new { id = participant.quizId });
            }
            //ViewData["quizId"] = new SelectList(_context.Quizzes, "id", "id", participant.quizId);
            return View(participant);
        }

        // GET: Participant/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participant = await _context.Participants
                .Include(p => p.quiz)
                .SingleOrDefaultAsync(m => m.id == id);
            if (participant == null)
            {
                return NotFound();
            }
            ViewData["quizId"] = participant.quizId;
            return View(participant);
        }

        // POST: Participant/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var participant = await _context.Participants.SingleOrDefaultAsync(m => m.id == id);
            _context.Participants.Remove(participant);
            await _context.SaveChangesAsync();
            //return RedirectToAction(nameof(Index));
            return RedirectToAction("ParticipantsList", "Quiz", new { id = participant.quizId });
        }

        private bool ParticipantExists(int id)
        {
            return _context.Participants.Any(e => e.id == id);
        }
    }
}
