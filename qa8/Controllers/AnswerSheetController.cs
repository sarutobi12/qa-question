﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using qa8.Data;
using qa8.Models;

namespace qa8.Controllers
{
    public class AnswerSheetController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AnswerSheetController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: AnswerSheet
        public async Task<IActionResult> Index()
        {
            return View(await _context.AnswerSheets.ToListAsync());
        }

        // GET: AnswerSheet/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var answerSheet = await _context.AnswerSheets
                .SingleOrDefaultAsync(m => m.id == id);
            if (answerSheet == null)
            {
                return NotFound();
            }

            return View(answerSheet);
        }

        // GET: AnswerSheet/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AnswerSheet/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,userId,quizId,questionId,questionType,choiceText,answerId,answerText,dateSubmitted,feedBack,Iscorrect,second")] AnswerSheet answerSheet)
        {
            if (ModelState.IsValid)
            {
                _context.Add(answerSheet);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(answerSheet);
        }

        // GET: AnswerSheet/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var answerSheet = await _context.AnswerSheets.SingleOrDefaultAsync(m => m.id == id);
            if (answerSheet == null)
            {
                return NotFound();
            }
            return View(answerSheet);
        }

        // POST: AnswerSheet/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,userId,quizId,questionId,questionType,choiceText,answerId,answerText,dateSubmitted,feedBack,Iscorrect,second")] AnswerSheet answerSheet)
        {
            if (id != answerSheet.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(answerSheet);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AnswerSheetExists(answerSheet.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(answerSheet);
        }

        // GET: AnswerSheet/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var answerSheet = await _context.AnswerSheets
                .SingleOrDefaultAsync(m => m.id == id);
            if (answerSheet == null)
            {
                return NotFound();
            }

            return View(answerSheet);
        }

        // POST: AnswerSheet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var answerSheet = await _context.AnswerSheets.SingleOrDefaultAsync(m => m.id == id);
            _context.AnswerSheets.Remove(answerSheet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AnswerSheetExists(int id)
        {
            return _context.AnswerSheets.Any(e => e.id == id);
        }
    }
}
