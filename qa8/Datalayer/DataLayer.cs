﻿using qa8.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace qa8.Datalayer
{
    public class DataLayer
    {
        public List<UserAccount> ReadSach()
        {

            List<UserAccount> allSach = new List<UserAccount>();

            var path = @"wwwroot\accountExcel\ds2.txt";

            List<string> lines = File.ReadAllLines(path).ToList();
            var i = 1;
            foreach (var line in lines)
            {
                string[] entries = line.Split(",");
                UserAccount sach = new UserAccount();
                //sach.ID = i;
                sach.cardId = entries[0];
                sach.fullName = entries[1];
                //sach.EnglishName = entries[2];
                //sach.LoaiSach = entries[3];
                //sach.ISBN = entries[4];
                //sach.SoLuong = entries[5];
                //sach.TienPhat = entries[6];
                allSach.Add(sach);
                i++;
            }

            return allSach;
        }
    }
}
