﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using qa8.Models;


namespace qa8.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<AnswerSheet> AnswerSheets { get; set; }
        public DbSet<Participant> Participants { get; set; }
        public DbSet<Idea> Idea { get; set; }
        public DbSet<Idea_User> Idea_User { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<Quiz>().ToTable("Quiz");
            builder.Entity<Question>().ToTable("Question");
            builder.Entity<AnswerSheet>().ToTable("AnswerSheet");
            builder.Entity<Participant>().ToTable("Participant");
            builder.Entity<ApplicationUser>().ToTable("AspUsers");
            builder.Entity<Idea_User>().ToTable("Idea_User");
            builder.Entity<Idea>().ToTable("Idea");

            builder.Entity<ApplicationUser>(b =>
            {
                b.Property(u => u.Email).HasColumnName("EmailAdress");
            });
        }


        public DbSet<qa8.Models.ApplicationUser> ApplicationUser { get; set; }


        public DbSet<qa8.Models.ApplicationRole> ApplicationRole { get; set; }


        
    }
}
