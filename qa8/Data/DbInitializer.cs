﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using qa8.Models;
using static qa8.Models.Question;

namespace qa8.Data
{
    public static class DbInitializer

    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            // Look for any Users.
            //if (context.Users.Any())
            //{
            //    return;   // DB has been seeded
            //}

            //var users = new User[]
            //{
            //new User{userName="Swook",passWord="",role=Role.admin},
            //new User{userName="Peter",passWord="",role=Role.admin},
            //new User{userName="Mei",passWord="",role=Role.admin},

            //};
            //foreach (User s in users)
            //{
            //    context.Users.Add(s);
            //}
            //context.SaveChanges();
            if (context.Quizzes.Any())
            {
                return;   // DB has been seeded
            }
            string dt = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us"));
            var quizzes = new Quiz[]
            {

            new Quiz{text="4M", deadLine = dt },
            new Quiz{text="test2",  deadLine = dt}

            };
            foreach (Quiz c in quizzes)
            {
                context.Quizzes.Add(c);
            }
            context.SaveChanges();


            var questions = new Question[]
            {
            new Question{text="question I ?",quizId=1,multipleChoice=true,A="answer 1",B="answer 2",correct=Correct.A},
            new Question{text="question II ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},
            new Question{text="question III ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},
            new Question{text="question IV ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},
            new Question{text="question V ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},
            new Question{text="question VI ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},
            new Question{text="question VII ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},
            new Question{text="question VIII ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},
            new Question{text="question IX ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},
            new Question{text="question X ?",quizId=1,multipleChoice=true,A="answer A",B="answer B",correct=Correct.A},


            new Question{text="question I ?",quizId=2,multipleChoice=true,A="answer 3",B="answer 4",C="answer 5",correct=Correct.B},
            new Question{text="question II ?",quizId=2,multipleChoice=false,},

            };
            foreach (Question c in questions)
            {
                context.Questions.Add(c);
            }
            context.SaveChanges();


            var answersheets = new AnswerSheet[]
            {
            new AnswerSheet{userId=1,quizId=1,questionId=1,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=2,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=3,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=4,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=5,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=6,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=7,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=8,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=10,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=1,quizId=1,questionId=9,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},

            new AnswerSheet{userId=2,quizId=1,questionId=1,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=2,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=3,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=4,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=5,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=6,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=7,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=8,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=10,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=2,quizId=1,questionId=9,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},


            new AnswerSheet{userId=3,quizId=1,questionId=1,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=2,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=3,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=4,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=5,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=6,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=7,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=8,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=10,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=3,quizId=1,questionId=9,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},

            new AnswerSheet{userId=4,quizId=1,questionId=1,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=2,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=3,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=4,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=5,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=6,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=7,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=8,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=10,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=4,quizId=1,questionId=9,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},


            new AnswerSheet{userId=5,quizId=1,questionId=1,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=2,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=3,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=4,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=5,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=6,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=7,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=8,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=10,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=5,quizId=1,questionId=9,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},


            new AnswerSheet{userId=6,quizId=1,questionId=1,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=2,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=3,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=4,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=5,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=6,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=7,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=8,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=10,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},
            new AnswerSheet{userId=6,quizId=1,questionId=9,answerText="blablabla",correctAnswer="blablabla",questionType=true,second="4",dateSubmitted=DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-us")),isCorrect=true,openAnswer="blablabla"},



            };
            foreach (AnswerSheet a in answersheets)
            {
                context.AnswerSheets.Add(a);
            }
            context.SaveChanges();

        }

    }
}
