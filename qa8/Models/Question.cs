﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using qa8.Models;

namespace qa8.Models
{
    
    public class Question
    {
        public enum Correct
        {
            A, B, C, D, E
        }
        [Display(Name = "Code")]
        public int id { get; set; }

        [Display(Name = "QuizID")]
        public int quizId { get; set; }

        [Display(Name = "Question")]
        public string text { get; set; }

        [Display(Name = "Choice A")]
        public string A { get; set; }

        [Display(Name = "Choice B")]
        public string B { get; set; }

        [Display(Name = "Choice C")]
        public string C { get; set; }

        [Display(Name = "Choice D")]
        public string D { get; set; }

        [Display(Name = "Choice E")]
        public string E { get; set; }


        [Display(Name = "Correct")]
        public Correct correct { get; set; }

        [Display(Name = "Answer choice")]
        public bool multipleChoice { get; set; }

        [Display(Name = "Question image")]
        public string imageSource { get; set; }



        public string imageSourceA { get; set; }
        public string imageSourceB { get; set; }
        public string imageSourceC { get; set; }
        public string imageSourceD { get; set; }
        public string imageSourceE { get; set; }


        public Quiz quiz { get; set; }
        public IEnumerable<AnswerSheet> answerSheets { get; set; }

    }
}