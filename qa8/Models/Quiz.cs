﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace qa8.Models
{

    public class Quiz
    {
        [Display(Name = "Code")]
        public int id { get; set; }

        [Display(Name = "Test name")]
        public string text { get; set; }

        [Display(Name = "Date")]
        public string deadLine { get; set; }

        [Display(Name = "Pass score")]
        public string targetScore { get; set; }

        [Display(Name = "Release")]
        public bool status { get; set; }

        [Display(Name = "ID card No.")]
        public string authorId { get; set; }

        [Display(Name = "Timer")]
        public string timer { get; set; }

        [Display(Name = "Show Result")]
        public bool showResult { get; set; }

        [Display(Name = "Shuffle the answer")]
        public bool ramdomizeOrderOfAnswer { get; set; }

        [Display(Name = "Shuffle the question")]
        public bool ramdomizeOrderOfQuestion { get; set; }

        



        public IEnumerable<Question> questions { get; set; }

        public IEnumerable<Participant> participants { get; set; }

    }
}
