﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace qa8.Models
{
    public class Participant
    {
        [Display(Name = "Code")]
        public int id { get; set; }
        [Display(Name = "Quiz ID")]
        public int? quizId { get; set; }
        [Display(Name = "Staff ID Card No.")]
        public string userId { get; set; }

        public bool alreadyDoneThisTest {get;set;}

        public Quiz quiz { get; set; }
        //public IEnumerable<ApplicationUser> user { get; set; }
    }
}
