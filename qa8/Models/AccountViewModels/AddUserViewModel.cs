﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace qa8.Models.AccountViewModels
{
    public class AddUserViewModel
    {
       
        [Required]
        [Display(Name = "ID Card Number")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string fullName { get; set; }


        [Required]
        [Display(Name = "Account Level")]
        public string accountLevel { get; set; }

    }
}
