﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static qa8.Models.Question;

namespace qa8.Models.ViewModel
{
    
        public class RawReport
        {
            public string userId { get; set; }
            public string userName { get; set; }
            public string question { get; set; }
            public string questionType { get; set; }
            public string choice { get; set; }
            
            public string correctanswer { get; set; }
            public string openAnswer { get; set; }
            public bool check { get; set; }
            public string time { get; set; }

    }
        public class UserViewModel
        {
            
            public int userId { get; set; }
            public string userFullName { get; set; }
            
        }

        public class UserInfo
        {
            public int id;
            public string userId { get; set; }
            public string userFullName { get; set; }
        }

        public class QuizViewModel
        {
            public int quizId { get; set; }
            public string quizName { get; set; }
            public DateTime releaseDate { get; set; }
            public DateTime deadLine { get; set; }
            public int userId { get; set; }
        
            public List<SelectListItem> listOfQuizz { get; set; }
            public List<QuestionViewModel> listofQuestion { get; set; }

        }

        public class QuestionViewModel
        {
            public int questionId { get; set; }

            public int userId { get; set; }
            
            public int quizId { get; set; }

            public string text { get; set; }

            public string A { get; set; }

            public string B { get; set; }

            public string C { get; set; }

            public string D { get; set; }

            public Correct correct { get; set; }

            public bool multipleChoice { get; set; }

            public string image { get; set; }

        }

        public class ChoiceViewModel
        {
            public int choiceId { get; set; }
            public int questionId { get; set; }
            public int authorId { get; set; }        
            public int quizId { get; set; }
            public string choiceText { get; set; }  
            public bool mark { get; set; }

        }

        public class QuizAnswersViewModel
        {
            public int questionId { get; set; }
            public string questionText { get; set; }
            public string questionType { get; set; }
            //public string AnswerOfQuestion { get; set; }
            public int choiceId { get; set; }
            public string choiceText { get; set; }
            public bool IsCorrect { get; set; }
            public string feedBack { get; set; }

        }
        public class AnswerSheetViewModel
        {
            public int Id { get; set; }
            public int actualChoiceId { get; set; }
            public int choiceId { get; set; }
            public int questionId { get; set; }
            public int userId { get; set; }
            public int quizId { get; set; }
            public string expectChoiceText { get; set; }
            public bool IsCorrect { get; set; }
            public string feedBack { get; set; }

           
        }

        public class QuizDetailViewModel
        {
            public int userId { get; set; }
            [Display(Name = "ID No.")]
            public string userName { get; set; }
            [Display(Name = "Full Name")]
            public string fullName { get; set; }
            [Display(Name = "Nick Name")]
            public string nickName { get; set; }
            [Display(Name = "DateTime Submitted")]
            public string submittionDateTime { get; set; }
            [Display(Name = "Score(%)")]
            public double score { get; set; }
            [Display(Name = "Result")]
            public string result { get; set; }
            public string CycleTime { get; set; }
            
        }
        public class Result
        {
            public int participants { get; set; }
            //public int userName { get; set; }
            public int averageScore { get; set; }
            public int highestScore { get; set; }
            public int lowestScore { get; set; }
            


        }
    //public class answers
    //{

    //}
    public class Answer
    {
        public string letter { get; set; }
        public string imagesource { get; set; }
        public string answer { get; set; }
    }
    
    public class questionVM
    {
        public int id { get; set; }
        public Answer [] answers { get; set; }
        //public correctVM correct { get; set; }
        public string correctAnswer;
        public int userId { get; set; }
        public string questiontext { get; set; }
        public int quizId { get; set; }
        public bool multiplechoice { get; set; }
        public string QuestionImage { get; set; }
        

    }

    public class DataVM
    {
       public List<questionVM>  questions { get; set; }
       public Quiz quizsetting { get; set; }
    }
    public class AnswerSheetVM
    {
        public Quiz quiz { get; set; }
        public List<questionVM> questions { get; set; }
        public string [] selections { get; set; }
        public string[] cycletime { get; set; }

    }
    public class UserAccount
    {
        public string cardId { get; set; }
        public string fullName { get; set; }
        public string EnglishName { get; set; }
    }

}
