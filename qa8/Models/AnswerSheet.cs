﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace qa8.Models
{
    public class AnswerSheet

    {
        [Display(Name = "Code")]
        public int id { get; set; }

        [Display(Name = "User code")]
        public int userId { get; set; }

        [Display(Name = "Quiz code")]
        public int quizId { get; set; }

        [Display(Name = "Question code")]
        public int questionId { get; set; }

        [Display(Name = "question type")]
        public bool questionType { get; set; }

        [Display(Name = "Actual Answer")]
        public string answerText { get; set; }

        //[Display(Name = "AnswerID")]
        //public string answerId { get; set; }

        [Display(Name = "Correct Answer")]
        public string correctAnswer { get; set; }

        [Display(Name = "Date submitted")]
        public string dateSubmitted { get; set; }

        [Display(Name = "Open answer")]
        public string openAnswer { get; set; }

        [Display(Name = "IsCorrect")]
        public bool isCorrect { get; set; }


        [Display(Name = "Seconds")]
        public string second { get; set; }

        public Question question { get; set; }

        public static implicit operator AnswerSheet(List<AnswerSheet> v)
        {
            throw new NotImplementedException();
        }
    }
}