﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qa8.Models
{
    public class Idea_User
    {
        public enum Important_Level{ high, medium, normal }
        public int id { get; set; }
        public int idea_Id { get; set; }
        public int user_Id  { get; set; }
        public Important_Level level { get; set; }

        public Idea Idea { get; set; }
    }
}
