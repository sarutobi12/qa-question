﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace qa8.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser<int>
    {
        // Your Extended Properties
        public string departmentCode { get; set; }
        public string cardNumber { get; set; }
        public string fullName { get; set; }
        public DateTime? startDate { get; set; }
        [Display(Name = "English Name")]
        public string nickName { get; set; }
        public string accountLevel { get; set; }

    }
}
