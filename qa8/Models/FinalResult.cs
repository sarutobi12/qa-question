﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qa8.Models
{
    public class FinalResult
    {
        public int id { get; set; }
        public string quizId { get; set; }
        public string userId { get; set; }
        public string score { get; set; }
        public string passFail { get; set; }
    }
}
