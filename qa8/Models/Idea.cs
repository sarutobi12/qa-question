﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace qa8.Models
{
    public class Idea
    {
        public int id { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string idea_Title { get; set; }
        [Required]
        [Display(Name = "Content")]
        public string idea_Content { get; set; }
        [Display(Name = "Date_Created")]
        public string date_Created { get; set; }
        public string ip_Created { get; set; }

        public IEnumerable<Idea_User> idea_Users { get; set; }
    }
}
