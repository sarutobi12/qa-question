﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace qa8.Migrations
{
    public partial class AddTimePropertyToQuiz : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participant_Quiz_quizId",
                table: "Participant");

            migrationBuilder.AddColumn<string>(
                name: "time",
                table: "Quiz",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "quizId",
                table: "Participant",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Participant_Quiz_quizId",
                table: "Participant",
                column: "quizId",
                principalTable: "Quiz",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participant_Quiz_quizId",
                table: "Participant");

            migrationBuilder.DropColumn(
                name: "time",
                table: "Quiz");

            migrationBuilder.AlterColumn<int>(
                name: "quizId",
                table: "Participant",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Participant_Quiz_quizId",
                table: "Participant",
                column: "quizId",
                principalTable: "Quiz",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
