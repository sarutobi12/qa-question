﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace qa8.Migrations
{
    public partial class addIdeaClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Idea",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    date_Created = table.Column<string>(nullable: true),
                    idea_Content = table.Column<string>(nullable: true),
                    idea_Title = table.Column<string>(nullable: true),
                    ip_Created = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Idea", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Idea_User",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ideaid = table.Column<int>(nullable: true),
                    idea_Id = table.Column<int>(nullable: false),
                    level = table.Column<int>(nullable: false),
                    user_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Idea_User", x => x.id);
                    table.ForeignKey(
                        name: "FK_Idea_User_Idea_Ideaid",
                        column: x => x.Ideaid,
                        principalTable: "Idea",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Idea_User_Ideaid",
                table: "Idea_User",
                column: "Ideaid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Idea_User");

            migrationBuilder.DropTable(
                name: "Idea");
        }
    }
}
