﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace qa8.Migrations
{
    public partial class uploadquestion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "image",
                table: "Question");

            migrationBuilder.RenameColumn(
                name: "filePath",
                table: "Question",
                newName: "imageSourceE");

            migrationBuilder.RenameColumn(
                name: "fileName",
                table: "Question",
                newName: "imageSourceD");

            migrationBuilder.AddColumn<string>(
                name: "imageSourceA",
                table: "Question",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "imageSourceB",
                table: "Question",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "imageSourceC",
                table: "Question",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "imageSourceA",
                table: "Question");

            migrationBuilder.DropColumn(
                name: "imageSourceB",
                table: "Question");

            migrationBuilder.DropColumn(
                name: "imageSourceC",
                table: "Question");

            migrationBuilder.RenameColumn(
                name: "imageSourceE",
                table: "Question",
                newName: "filePath");

            migrationBuilder.RenameColumn(
                name: "imageSourceD",
                table: "Question",
                newName: "fileName");

            migrationBuilder.AddColumn<byte[]>(
                name: "image",
                table: "Question",
                nullable: true);
        }
    }
}
