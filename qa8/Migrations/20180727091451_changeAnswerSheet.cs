﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace qa8.Migrations
{
    public partial class changeAnswerSheet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "answerId",
                table: "AnswerSheet");

            migrationBuilder.RenameColumn(
                name: "Iscorrect",
                table: "AnswerSheet",
                newName: "isCorrect");

            migrationBuilder.RenameColumn(
                name: "feedBack",
                table: "AnswerSheet",
                newName: "openAnswer");

            migrationBuilder.RenameColumn(
                name: "choiceText",
                table: "AnswerSheet",
                newName: "correctAnswer");

            migrationBuilder.AlterColumn<string>(
                name: "second",
                table: "AnswerSheet",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "dateSubmitted",
                table: "AnswerSheet",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isCorrect",
                table: "AnswerSheet",
                newName: "Iscorrect");

            migrationBuilder.RenameColumn(
                name: "openAnswer",
                table: "AnswerSheet",
                newName: "feedBack");

            migrationBuilder.RenameColumn(
                name: "correctAnswer",
                table: "AnswerSheet",
                newName: "choiceText");

            migrationBuilder.AlterColumn<int>(
                name: "second",
                table: "AnswerSheet",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "dateSubmitted",
                table: "AnswerSheet",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "answerId",
                table: "AnswerSheet",
                nullable: true);
        }
    }
}
