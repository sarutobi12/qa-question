﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace qa8.Migrations
{
    public partial class addnickName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participant_Quiz_quizId",
                table: "Participant");

            migrationBuilder.AlterColumn<int>(
                name: "quizId",
                table: "Participant",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "nickName",
                table: "AspUsers",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Participant_Quiz_quizId",
                table: "Participant",
                column: "quizId",
                principalTable: "Quiz",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participant_Quiz_quizId",
                table: "Participant");

            migrationBuilder.DropColumn(
                name: "nickName",
                table: "AspUsers");

            migrationBuilder.AlterColumn<int>(
                name: "quizId",
                table: "Participant",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Participant_Quiz_quizId",
                table: "Participant",
                column: "quizId",
                principalTable: "Quiz",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
