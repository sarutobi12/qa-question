﻿using qa8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qa8.Repository
{
    public class AccountPolicy
    {


        public bool CheckAccountLevel(ApplicationUser currentuser, ApplicationUser user)
        {
            bool check = false;

            if (Convert.ToInt32(currentuser.accountLevel) >= Convert.ToInt32(user.accountLevel) )
            {
                check = true;
            }
            else
            {
                check = false;
            }

            return check;
        }

        public bool CheckAccountLevelVsControllerPolicy(ApplicationUser currentuser, int controllerPolicy)
        {
            bool check = false;

            if (Convert.ToInt32(currentuser.accountLevel) >= controllerPolicy)
            {
                check = true;
            }
            else
            {
                check = false;
            }

            return check;
        }

        public bool CheckTheOwnerOrAdminPolicy (ApplicationUser currentuser, Quiz quiz)
        {
            bool check = false;

            if (currentuser.UserName == quiz.authorId || Convert.ToInt32(currentuser.accountLevel) >= 4)
            {
                check = true;
            }
            else
            {
                check = false;
            }

            return check;
        }


    }
}
