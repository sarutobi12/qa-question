﻿var quizconfig = {
    pageSize : 3,
    pageIndex : 1,
};
var quizcontroller = {
    init: function () {
        quizcontroller.loadData();
        quizcontroller.registerEvent();
    },
    registerEvent: function () {
        $('.Deadline').off('keypress').on('keypress', function (e) {
            if (e.which == 13)
            {
                var id = $(this).data('id');
                var datetime = $(this).val();
                var score = $(this).data('container');
                quizcontroller.updateData(id, datetime, score);
            }
        });

        $('.TargetScore').off('keypress').on('keypress', function (e) {
            if (e.which == 13) {
                var id = $(this).data('id');
                var scoreinput = $(this).val();   
                var datetime = $(this).data('container');
                quizcontroller.updateData(id, datetime, scoreinput);
            }
        });

        $('#btnAddnew').off('click').on('click', function (e) {
            $('#ModalAddUpdate').modal('show');
            quizcontroller.resetForm();
        });

        $('#btnSave').off('click').on('click', function (e) {
            quizcontroller.saveData();
        });

        $('#btnEdit').off('click').on('click', function (e) {
            $('#ModalAddUpdate').modal('show');
            var id = $(this).data('id');
            quizcontroller.loadDetail(id);
        });
    },
    loadDetail: function (id) {
        $.ajax({
            url: '/Quiz/GetDetail',
            type: 'GET',
            dataType: 'json',
            data: { /*quiz: JSON.stringify(data)*/
                id: id,
            },
            success: function (response) {
                if (response.status) {
                    var data = response.data;
                    $('#quizId').val(data.quizId);
                    $('#quizName').val(data.quizName);
                    $('#authorId').val(data.authorId);
                    $('#deadLine').val(data.deadLine);
                    $('#score').val(data.score);
                }
                else {
                    alert(response.message);
                }
            },
            err: function (err) {
                console.log(err);
            },
        });

    },

    saveData : function () {
        var data = {
            
            quizName: $('#quizName').val(),
            authorId: $('#authorId').val(),
            deadLine: $('#deadLine').val(),
            score: $('#score').val(),
            quizId: parseInt($('#quizID').val()),
        };
        $.ajax({
            url: '/Quiz/SaveData',
            type: 'POST',
            dataType:'json',
            data: { /*quiz: JSON.stringify(data)*/
                quiz: JSON.stringify( data),
            },
            success: function (response) {
                if (response.status ) {
                    alert('You have successfully created');
                    $('#ModalAddUpdate').modal('hide');
                    quizcontroller.loadData();
                }
                else {
                    alert(response.message);
                }
            },
            err: function (err) {
                console.log(err);
            },
        }); 
    },

    resetForm: function () {
        $('#quizId').val(0);
        $('#quizName').val('');
        $('#authorId').val(0);
        $('#deadLine').val();
        $('#score').val(0);

        //$('#ckstatus').prop('check',true);

    },

    updateData: function (id, datetime, scoreinput) {
        var data = {
            
            quizId : id,
            deadLine: datetime,
            score: scoreinput,
        };
        $.ajax({
            url: '/Quiz/UpdateData',
            type: 'POST',
            data: { quiz: JSON.stringify(data) },
            success: function (response) {
                if (response.status) {
                    alert('You have successfully updated');

                }
                else
                {
                    alert(response.message);
                }
            },
            err: function (err) {
                console.log(err);
            },
        });
    },
 
    loadData: function () {
        $.ajax ({
            url: '/Quiz/LoadData',
            type: 'GET',
            dataType: 'json',
            data: {
                page: quizconfig.pageIndex,
                pageSize: quizconfig.pageSize,
            },
            success: function (response){
                if (response.status == true)
                {
                    var data = response.data;
                    var html = '';
                    var template = $('#data-template').html();
                    $.each(data, function (i, item) {
                        html += Mustache.render(template, 
                            {
                                Code: item.quizId,
                                Quiz: item.quizName,
                                Deadline: item.deadLine,
                                TargetScore: item.score,

                            });
                    });

                    $('#tableData').html(html);
                    quizcontroller.paging(response.total, function () {
                        quizcontroller.loadData();
                    });
                    quizcontroller.registerEvent();
                }
            }
        })
    },

    paging: function (totalRow, callback) {
        var totalPage = Math.ceil(totalRow / quizconfig.pageSize);
        $('.pagination').twbsPagination({
            totalPages: totalPage,
            visiblePages: 10,
            onPageClick: function (event, page) {
                quizconfig.pageIndex = page;
                setTimeout(callback, 200);
            },
        });
    },
}
quizcontroller.init();